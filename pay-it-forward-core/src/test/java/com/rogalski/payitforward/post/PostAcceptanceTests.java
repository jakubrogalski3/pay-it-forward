package com.rogalski.payitforward.post;

import com.rogalski.payitforward.exception.notfound.TagNotFoundException;
import com.rogalski.payitforward.model.Tag;
import com.rogalski.payitforward.model.User;
import com.rogalski.payitforward.repository.TagRepository;
import com.rogalski.payitforward.repository.UserRepository;
import com.rogalski.payitforward.service.PostService;
import com.rogalski.payitforward.service.SavePostService;
import com.rogalski.payitforward.service.UserService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.persistence.EntityManager;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PostAcceptanceTests {

    private final static String PATH = "/posts/";
    private final String USER_ID_HEADER = "user_id";
    private final String POST_CONTENT = "post content testing";
    @Autowired
    PostService postService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    SavePostService savePostService;
    @MockBean
    EntityManager entityManager;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getUserFeed () throws Exception {
        this.mockMvc
                .perform(get(PATH + "feed").header(USER_ID_HEADER, 1))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldCreateGlobalPost () throws Exception {
        AtomicInteger createdPostId = new AtomicInteger();
        User user = userService.getUserById(1L);
        Tag tag = tagRepository
                .findById(1L)
                .orElseThrow(() -> new TagNotFoundException(new Object[]{
                        MessageFormat.format("tagId: {0}", 1L)
                }));
        this.mockMvc
                .perform(post(PATH)
                        .content(createPostData())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(USER_ID_HEADER, 1))
                .andExpect(status().isOk())
                .andDo(result -> createdPostId.set(new JSONObject(result
                        .getResponse()
                        .getContentAsString()).getInt("id")));

        MvcResult result = this.mockMvc
                .perform(get(PATH + createdPostId.get()).header(USER_ID_HEADER, 1))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.id", is(createdPostId.get())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.username", is(user.getUsername())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.content", is(POST_CONTENT)));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.tag", is(tag.getName())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.group", nullValue()));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.comments", empty()));
    }

    @Test
    public void shouldCreateGlobalPostWithoutTagShouldReturn400 () throws Exception {
        this.mockMvc
                .perform(post(PATH)
                        .content(createPostDataWithoutTagId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(USER_ID_HEADER, 1))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void wrongTagId_shouldReturn400 () throws Exception {
        List<Tag> allTags = tagRepository.findAll();
        Long lastTagId = allTags
                .get(allTags.size() - 1)
                .getId() + 1000;
        this.mockMvc
                .perform(post(PATH)
                        .content(createPostDataWithWrongTagId(lastTagId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(USER_ID_HEADER, 1))
                .andExpect(status().isBadRequest());
    }

    String createPostData () throws JSONException {
        return new JSONObject()
                .put("content", POST_CONTENT)
                .put("tagId", 1)
                .toString();
    }

    String createPostDataWithoutTagId () throws JSONException {
        return new JSONObject()
                .put("content", POST_CONTENT)
                .toString();
    }

    String createPostDataWithWrongTagId (Long tagId) throws JSONException {
        return new JSONObject()
                .put("content", POST_CONTENT)
                .put("tagId", tagId)
                .toString();
    }
}
