package com.rogalski.payitforward.grouppost;

import com.rogalski.payitforward.exception.notfound.GroupNotFoundException;
import com.rogalski.payitforward.exception.notfound.TagNotFoundException;
import com.rogalski.payitforward.model.Group;
import com.rogalski.payitforward.model.GroupTag;
import com.rogalski.payitforward.model.User;
import com.rogalski.payitforward.repository.GroupRepository;
import com.rogalski.payitforward.repository.GroupTagRepository;
import com.rogalski.payitforward.service.UserService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.text.MessageFormat;
import java.util.concurrent.atomic.AtomicInteger;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GroupPostAcceptanceTest {

    private final static String PATH = "/group-posts/groups/";

    private final String USER_ID_HEADER = "user_id";
    private final String USER_ROLES_HEADER = "roles";

    private final String POST_CONTENT = "post content testing";
    @Autowired
    UserService userService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private GroupTagRepository groupTagRepository;

    @Test
    public void createGroupPost () throws Exception {
        AtomicInteger createdPostId = new AtomicInteger();
        User user = userService.getUserById(1L);
        Group group = groupRepository
                .findById(1L)
                .orElseThrow(() -> new GroupNotFoundException(new Object[]{
                        MessageFormat.format("groupId: {0}", 1L)
                }));
        this.mockMvc
                .perform(post(PATH + "1/")
                        .content(createPostData())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(USER_ID_HEADER, 1)
                        .header(USER_ROLES_HEADER, "GROUP_1_MEMBER"))
                .andExpect(status().isOk())
                .andDo(result -> createdPostId.set(new JSONObject(result
                        .getResponse()
                        .getContentAsString()).getInt("id")));

        MvcResult result = this.mockMvc
                .perform(get(PATH + group.getId() + "/posts/" + createdPostId.get())
                        .header(USER_ID_HEADER, 1)
                        .header(USER_ROLES_HEADER, "GROUP_1_MEMBER"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.id", is(createdPostId.get())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.username", is(user.getUsername())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.content", is(POST_CONTENT)));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.tag", nullValue()));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.group", is(group.getName())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.comments", empty()));
    }

    @Test
    public void createGroupPostWithTag () throws Exception {
        AtomicInteger createdPostId = new AtomicInteger();
        User user = userService.getUserById(1L);
        Group group = groupRepository
                .findById(1L)
                .orElseThrow(() -> new GroupNotFoundException(new Object[]{
                        MessageFormat.format("groupId: {0}", 1L)
                }));
        GroupTag tag = groupTagRepository
                .findById(1L)
                .orElseThrow(() -> new TagNotFoundException(new Object[]{
                        MessageFormat.format("tagId: {0}", 1L)
                }));

        this.mockMvc
                .perform(post(PATH + "1/")
                        .content(createPostDataWithTagId(tag.getId()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(USER_ID_HEADER, 1)
                        .header(USER_ROLES_HEADER, "GROUP_1_MEMBER"))
                .andExpect(status().isOk())
                .andDo(result -> createdPostId.set(new JSONObject(result
                        .getResponse()
                        .getContentAsString()).getInt("id")));

        MvcResult result = this.mockMvc
                .perform(get(PATH + group.getId() + "/posts/" + createdPostId.get())
                        .header(USER_ID_HEADER, 1)
                        .header(USER_ROLES_HEADER, "GROUP_1_MEMBER"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.id", is(createdPostId.get())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.username", is(user.getUsername())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.content", is(POST_CONTENT)));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.tag", is(tag.getName())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.group", is(group.getName())));
        assertThat(result
                .getResponse()
                .getContentAsString(), hasJsonPath("@.comments", empty()));
    }

    String createPostData () throws JSONException {
        return new JSONObject()
                .put("content", POST_CONTENT)
                .toString();
    }

    String createPostDataWithTagId (Long tagId) throws JSONException {
        return new JSONObject()
                .put("content", POST_CONTENT)
                .put("tagId", tagId)
                .toString();
    }

}
