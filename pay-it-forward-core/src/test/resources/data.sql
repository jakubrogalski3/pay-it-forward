INSERT INTO public.pif_user (id, created_on, first_name, last_name, password, username, uuid, email, is_enabled,
                             identity_provider)
VALUES (1, '2021-04-19 17:41:37.000000', 'Jakub', 'Rogalski',
        '$2a$10$0kESOz/ol1Gluqh74U/oCul/0cRz/u3Ok5Z04XCU99lD8r5jv1kMe', 'kubarogal17', '1', 'kubarogal17@gmail.com',
        true, 'LOCAL');
INSERT INTO public.pif_user (id, created_on, first_name, last_name, password, username, uuid, email, is_enabled,
                             identity_provider)
VALUES (19, '2021-05-02 15:03:37.811000', 'Jakub', 'Rogalski', null, 'jakubrogalski3@gmail.com',
        '9bd78b97-b899-4abd-ad45-feaf1448a1a9', 'jakubrogalski3@gmail.com', true, 'GOOGLE');
INSERT INTO public.pif_user (id, created_on, first_name, last_name, password, username, uuid, email, is_enabled,
                             identity_provider)
VALUES (21, '2021-07-14 20:24:48.983000', 'Test', 'Testowy',
        '$2a$10$6MwW7HPzuIzlX7DIMi7wye.6k0vqyvNC8o.DxjCTUZOpB6CGffvj2', null, '8b43d780-c228-4ff8-91de-5ce7105cd8da',
        'test@aa', false, 'LOCAL');
INSERT INTO public.pif_user (id, created_on, first_name, last_name, password, username, uuid, email, is_enabled,
                             identity_provider)
VALUES (22, '2021-07-14 20:26:10.473000', 'Test', 'Testowy',
        '$2a$10$vW4nL4sL9oxd4rKIm2bg5.a/GMgZfhhCoWBG9xEJaanRsDbsZq5ki', null, '44703cb2-ea99-47ba-885b-424bbd271452',
        'test@aa', false, 'LOCAL');
INSERT INTO public.pif_user (id, created_on, first_name, last_name, password, username, uuid, email, is_enabled,
                             identity_provider)
VALUES (20, '2021-07-14 20:23:00.251000', 'Test', 'Testowy',
        '$2a$10$W/OwSOJQDJNSWVL0gUG8vODYjgSXer.0Q9ZiRwLZJ9vop3//r64O.', 'testowy',
        '3077451f-6ac7-4711-bc8d-8c99b5b9009b', 'test@aa', false, 'LOCAL');

INSERT INTO public.system_permission (id, description, permission, uuid)
VALUES (1, 'system admin', 'SYSTEM_ADMIN', '7beadc20-1657-493a-b8a0-298c1ead4212');
INSERT INTO public.system_permission (id, description, permission, uuid)
VALUES (2, 'system user', 'SYSTEM_USER', '7beadc20-1657-493a-b8a0-298c1ead4211');

INSERT INTO public.tag (id, created_on, description, image, name, uuid)
VALUES (1, '2021-04-21 18:10:34.000000', 'tag desc', null, 'tag', '7beadc20-1657-493a-b8a0-298c1ead4211');
INSERT INTO public.tag (id, created_on, description, image, name, uuid)
VALUES (2, '2021-06-07 22:18:17.000000', 'uga buga', null, 'ungabunga', '7beadc20-1657-493a-b8a0-298c1ead4211');

INSERT INTO public.post (id, content, created_on, uuid, tag_id, user_id)
VALUES (1, 'daw', '2021-04-21 18:00:08.000000', '14dd64c2-a2c0-11eb-bcbc-0242ac130002', 1, 1);
INSERT INTO public.post (id, content, created_on, uuid, tag_id, user_id)
VALUES (2, 'awdfz', '2021-04-21 18:00:24.000000', '14dd64c2-a2c0-11eb-bcbc-0242ac130001', 1, 1);
INSERT INTO public.post (id, content, created_on, uuid, tag_id, user_id)
VALUES (5, 'post testing', '2021-07-27 17:50:24.463000', '6a79c0aa-baf8-4dcd-a138-7f277bf7a844', 2, 19);
INSERT INTO public.post (id, content, created_on, uuid, tag_id, user_id)
VALUES (6, 'procedure test', '2021-07-27 17:50:24.463000', '6a79c0aa-baf8-4dcd-a138-7f277bf7a845', 2, 19);
