package com.rogalski.payitforward.controller;

import com.rogalski.payitforward.dto.group.GroupDTO;
import com.rogalski.payitforward.dto.group.GroupDetailsDTO;
import com.rogalski.payitforward.service.GroupService;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController()
@RequestMapping("/groups")
@Api(tags = "Groups")
public class GroupController {

    private final GroupService groupService;

    public GroupController (GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("/{groupId}")
    @PreAuthorize("hasPermission(#groupId, 'GROUP', 'MEMBER')")
    public GroupDetailsDTO getGroupById (@PathVariable Long groupId) {
        return null;
    }

    @GetMapping("")
    public List<GroupDTO> getUserGroups () {
        return List.of(new GroupDTO(2L, "grp"));
    }

}
