package com.rogalski.payitforward.controller;

import com.rogalski.payitforward.dto.PageResponse;
import com.rogalski.payitforward.dto.post.CreateGroupPostDTO;
import com.rogalski.payitforward.dto.post.PostDTO;
import com.rogalski.payitforward.dto.post.UpdateGroupPostDTO;
import com.rogalski.payitforward.service.DeletePostService;
import com.rogalski.payitforward.service.GroupPostService;
import com.rogalski.payitforward.service.SavePostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController()
@RequestMapping("/group-posts")
@Api(tags = "Group Posts")
public class GroupPostController {

    private final GroupPostService groupPostService;
    private final SavePostService savePostService;
    private final DeletePostService deletePostService;

    public GroupPostController (GroupPostService groupPostService, SavePostService savePostService,
                                DeletePostService deletePostService) {
        this.groupPostService = groupPostService;
        this.savePostService = savePostService;
        this.deletePostService = deletePostService;
    }

    @GetMapping("/{groupId}")
    @PreAuthorize("hasPermission(#groupId, 'GROUP', 'MEMBER')")
    @ApiOperation(value = "Get posts by group.",
            notes = "Get all posts by group with possible search by user or/and tag.", response = PostDTO.class)
    public ResponseEntity<PageResponse<PostDTO>> getPostsByGroup (@PathVariable Long groupId,
                                                                  @RequestParam(name = "userId", required = false)
                                                                  Long userId,
                                                                  @RequestParam(name = "tagId", required = false)
                                                                  Long tagId, Pageable pageable) {
        PageResponse<PostDTO> posts = groupPostService.getPostsFromGroup(groupId, userId, tagId, pageable);
        return new ResponseEntity<>(posts, posts != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/groups/{groupId}/posts/{postId}")
    @PreAuthorize("hasPermission(#groupId, 'GROUP', 'MEMBER')")
    @ApiOperation(value = "Get group post by id.", notes = "Get group post by id.", response = PostDTO.class)
    public ResponseEntity<PostDTO> getGroupPostById (@PathVariable Long groupId, @PathVariable Long postId) {
        PostDTO posts = groupPostService.getGroupPostDtoById(postId);
        return new ResponseEntity<>(posts, posts != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/groups/{groupId}")
    @PreAuthorize("hasPermission(#groupId, 'GROUP', 'MEMBER')")
    @ApiOperation(value = "Create post in group.", notes = "Create post in group with or without tag.",
            response = PostDTO.class)
    public ResponseEntity<PostDTO> createGroupPost (@PathVariable Long groupId,
                                                    @RequestBody @Valid CreateGroupPostDTO postDto) {
        PostDTO createdPost = savePostService.createGroupPost(groupId, postDto);
        return new ResponseEntity<>(createdPost, HttpStatus.OK);
    }

    @DeleteMapping("/groups/{groupId}/posts/{postId}")
    @PreAuthorize("hasPermission(#groupId, 'GROUP', 'MEMBER')")
    @ApiOperation(value = "Delete group post.", notes = "Delete group post.")
    public ResponseEntity<Void> deleteGroupPost (@PathVariable Long groupId, @PathVariable Long postId) {
        deletePostService.deleteGroupPost(postId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PatchMapping("/groups/{groupId}/posts/{postId}")
    @PreAuthorize("hasPermission(#groupId, 'GROUP', 'MEMBER')")
    @ApiOperation(value = "Update group post content.", notes = "Update group post content.")
    public ResponseEntity<PostDTO> updateGroupPost (@PathVariable Long groupId, @PathVariable Long postId,
                                                    UpdateGroupPostDTO updateGroupPostDTO) {
        return new ResponseEntity<>(savePostService.updateGroupPost(postId, updateGroupPostDTO), HttpStatus.OK);
    }

}
