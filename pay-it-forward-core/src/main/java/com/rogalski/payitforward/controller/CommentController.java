package com.rogalski.payitforward.controller;

import com.rogalski.payitforward.dto.comment.AddCommentDTO;
import com.rogalski.payitforward.dto.post.PostDTO;
import com.rogalski.payitforward.service.SaveCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comments")
@Api(tags = "Comments")
public class CommentController {

    private final SaveCommentService saveCommentService;

    public CommentController (SaveCommentService saveCommentService) {
        this.saveCommentService = saveCommentService;
    }

    @PostMapping()
    @ApiOperation(value = "Add comment.", notes = "Add comment to existing post.", response = PostDTO[].class)
    public void addComment (@RequestBody AddCommentDTO commentDTO) {
        saveCommentService.saveComment(commentDTO);
    }

}
