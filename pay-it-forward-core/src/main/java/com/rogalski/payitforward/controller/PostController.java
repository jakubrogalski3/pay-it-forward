package com.rogalski.payitforward.controller;

import com.rogalski.payitforward.dto.PageResponse;
import com.rogalski.payitforward.dto.post.CreateGlobalPostDTO;
import com.rogalski.payitforward.dto.post.PostDTO;
import com.rogalski.payitforward.dto.post.PostFeedDTO;
import com.rogalski.payitforward.dto.post.UpdatePostDTO;
import com.rogalski.payitforward.security.AuthenticationFacade;
import com.rogalski.payitforward.service.DeletePostService;
import com.rogalski.payitforward.service.PostService;
import com.rogalski.payitforward.service.SavePostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController()
@RequestMapping("/posts")
@Api(tags = "Posts")
public class PostController {

    private final PostService postService;
    private final SavePostService savePostService;
    private final DeletePostService deletePostService;

    public PostController (PostService postService, SavePostService savePostService,
                           DeletePostService deletePostService) {
        this.postService = postService;
        this.savePostService = savePostService;
        this.deletePostService = deletePostService;
    }

    @GetMapping("/feed")
    @ApiOperation(value = "Get posts for user.", notes = "Get posts for user from user groups and tags.",
            response = PostDTO[].class)
    public ResponseEntity<PageResponse<PostFeedDTO>> getPostsFeed (Pageable pageable) {
        return ResponseEntity.ok(postService.getPostFeedForUser(pageable));
    }

    @GetMapping("/tags/{tagId}")
    @ApiOperation(value = "Get posts by tag.", notes = "Get posts from tag if user has access to the tag.",
            response = PostDTO[].class)
    public ResponseEntity<PageResponse<PostDTO>> getPostsByTag (@PathVariable Long tagId, Pageable pageable) {
        return ResponseEntity.ok(postService.getPostsByTag(tagId, pageable));
    }

    @GetMapping("/users/{userId}")
    @PreAuthorize("permitAll()")
    @ApiOperation(value = "Get user posts.", notes = "Get posts added by user globally from all tags.",
            response = PostDTO[].class)
    public ResponseEntity<PageResponse<PostFeedDTO>> getUserGlobalPosts (@PathVariable Long userId, Pageable pageable) {
        return ResponseEntity.ok(postService.getUserGlobalPosts(userId, pageable));
    }
 
    @GetMapping("/users/myself")
    @ApiOperation(value = "Get user posts.", notes = "Get posts added by user globally from all tags.",
            response = PostDTO[].class)
    public ResponseEntity<PageResponse<PostFeedDTO>> getUserGlobalPosts (Pageable pageable) {
        return ResponseEntity.ok(postService.getUserGlobalPosts(AuthenticationFacade
                .getAuthentication()
                .getId(), pageable));
    }

    @GetMapping("/users/{userId}/tags/{tagId}")
    @ApiOperation(value = "Get user global posts by tag.", notes = "Get posts added by user in the tag.",
            response = PostDTO[].class)
    public ResponseEntity<PageResponse<PostDTO>> getUserTagPosts (@PathVariable Long userId, @PathVariable Long tagId,
                                                                  Pageable pageable) {
        return ResponseEntity.ok(postService.getUserTagPosts(tagId, userId, pageable));
    }

    @GetMapping("/{postId}")
    @ApiOperation(value = "Get post by id.", notes = "Get post by id.", response = PostDTO.class)
    public ResponseEntity<PostDTO> getPostById (@PathVariable Long postId) {
        PostDTO postDTO = postService.getPostDtoById(postId);
        return new ResponseEntity<>(postDTO, postDTO != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/")
    @ApiOperation(value = "Create global post.", notes = "Create global post with tag.", response = PostDTO.class)
    public ResponseEntity<PostDTO> createGlobalPost (@RequestBody @Valid CreateGlobalPostDTO postDto) {
        PostDTO createdPost = savePostService.createGlobalPost(postDto);
        return new ResponseEntity<>(createdPost, createdPost != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{postId}")
    @ApiOperation(value = "Delete global post.", notes = "Delete post.")
    public ResponseEntity<Void> deletePost (@PathVariable Long postId) {
        deletePostService.deleteGlobalPost(postId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PatchMapping("/{postId}")
    @ApiOperation(value = "Update global post.", notes = "Update post.")
    public ResponseEntity<PostDTO> updateGlobalPost (@PathVariable Long postId, UpdatePostDTO postDTO) {
        return new ResponseEntity<>(savePostService.updatePost(postId, postDTO), HttpStatus.OK);
    }

}
