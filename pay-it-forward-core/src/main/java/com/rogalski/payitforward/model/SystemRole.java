package com.rogalski.payitforward.model;

public enum SystemRole {
    ROLE_ADMIN, ROLE_USER
}
