package com.rogalski.payitforward.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Comment extends Content {

    @ManyToOne()
    private Post post;

    private Long parentId;
}
