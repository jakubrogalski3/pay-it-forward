package com.rogalski.payitforward.model;

public enum TagRole {
    ADMIN, MEMBER
}
