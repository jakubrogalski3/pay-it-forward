package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "pif_like")
@Data
public class Like {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private final UUID uuid = UUID.randomUUID();

    @ManyToOne()
    private User user;

    @ManyToOne()
    private Post post;

    private Date createdOn = new Date();

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Like))
            return false;
        Like like = (Like) o;
        return getUuid().equals(like.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
