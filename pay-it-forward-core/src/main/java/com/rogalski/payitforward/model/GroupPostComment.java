package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Data
public class GroupPostComment extends Content {

    @ManyToOne()
    private GroupPost post;
}
