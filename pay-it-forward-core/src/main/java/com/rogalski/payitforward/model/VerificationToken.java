package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class VerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String token;

    @OneToOne()
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    private Date expiryDate;

}
