package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "pif_user")
//@NamedEntityGraphs({
//        @NamedEntityGraph(
//                name = "graph.UserSystemPermission",
//                attributeNodes = {
//                        @NamedAttributeNode(value = "permission"),
//                        @NamedAttributeNode(value = "groups", subgraph = "groupPermissions"),
//                        @NamedAttributeNode(value = "tags", subgraph = "tagPermissions")
//                },
//                subgraphs = {
//                        @NamedSubgraph(name = "groupPermissions", attributeNodes = @NamedAttributeNode("groupPermissions")),
//                        @NamedSubgraph(name = "tagPermissions", attributeNodes = @NamedAttributeNode("tagPermissions"))
//                })
//})
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private final UUID uuid = UUID.randomUUID();

    private boolean isEnabled;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private IdentityProvider identityProvider;

    @Column(nullable = false, unique = true)
    private String email;

    private final Date createdOn = new Date();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy(value = "joinOn DESC")
    private Set<GroupMember> groups;

    @OneToMany()
    @JoinTable(name = "user_permission")
    private Set<SystemPermission> permission;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<TagMember> tags;

    //    @OneToMany(
    //            mappedBy = "user",
    //            cascade = CascadeType.ALL,
    //            orphanRemoval = true)
    //    @OrderBy(value = "createdOn DESC")
    //    private Set<Like> likes;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UserBlockedTag> blockedTags;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UserGroupBlockedTag> groupBlockedTags;

    public void addBlockedTag (UserBlockedTag userBlockedTag) {
        blockedTags.add(userBlockedTag);
    }

    public void removeBlockedTag (UserBlockedTag userBlockedTag) {
        blockedTags.remove(userBlockedTag);
    }

    public void addGroupBlockedTag (UserGroupBlockedTag userGroupBlockedTag) {
        groupBlockedTags.add(userGroupBlockedTag);
    }

    public void removeGroupBlockedTag (UserGroupBlockedTag userGroupBlockedTag) {
        groupBlockedTags.remove(userGroupBlockedTag);
    }

    public void removeGroupBlockedTags (List<UserGroupBlockedTag> userGroupBlockedTag) {
        groupBlockedTags.removeAll(userGroupBlockedTag);
    }

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof User))
            return false;
        User user = (User) o;
        return getUuid().equals(user.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
