package com.rogalski.payitforward.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Post extends Content {

    @ManyToOne()
    private Tag tag;

    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy(value = "createdOn DESC")
    private Set<Comment> comments;

}
