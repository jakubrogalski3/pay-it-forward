package com.rogalski.payitforward.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
public class UserBlockedTag {

    @EmbeddedId
    private UserBlockedTagId id;

    private final UUID uuid = UUID.randomUUID();

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("tagId")
    private Tag tag;

    @Data
    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UserBlockedTagId implements Serializable {
        Long userId;
        Long tagId;
    }

    public UserBlockedTag (User user, Tag tag) {
        this.id = new UserBlockedTagId(user.getId(), tag.getId());
        this.user = user;
        this.tag = tag;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof UserBlockedTag))
            return false;
        UserBlockedTag that = (UserBlockedTag) o;
        return getUuid().equals(that.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
