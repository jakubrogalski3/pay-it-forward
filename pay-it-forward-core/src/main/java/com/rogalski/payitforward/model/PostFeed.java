package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "findPostFeedByUserId", procedureName = "get_user_post_feed",
                resultClasses = PostFeed.class, parameters = {
                @StoredProcedureParameter(name = "userId", mode = ParameterMode.IN, type = Long.class),
                @StoredProcedureParameter(name = "limit", mode = ParameterMode.IN, type = Integer.class),
                @StoredProcedureParameter(name = "offset", mode = ParameterMode.IN, type = Long.class)
        }),
        @NamedStoredProcedureQuery(name = "findPostFeedCountByUserId", procedureName = "get_user_post_feed_count",
                parameters = {
                        @StoredProcedureParameter(name = "userId", mode = ParameterMode.IN, type = Long.class)
                }),
        @NamedStoredProcedureQuery(name = "findUserPosts", procedureName = "get_user_posts",
                resultClasses = PostFeed.class, parameters = {
                @StoredProcedureParameter(name = "userId", mode = ParameterMode.IN, type = Long.class),
                @StoredProcedureParameter(name = "viewerId", mode = ParameterMode.IN, type = Long.class),
                @StoredProcedureParameter(name = "limit", mode = ParameterMode.IN, type = Integer.class),
                @StoredProcedureParameter(name = "offset", mode = ParameterMode.IN, type = Long.class)
        }),
        @NamedStoredProcedureQuery(name = "findUserPostsCount", procedureName = "get_user_posts_count", parameters = {
                @StoredProcedureParameter(name = "userId", mode = ParameterMode.IN, type = Long.class),
                @StoredProcedureParameter(name = "viewerId", mode = ParameterMode.IN, type = Long.class)
        })
})
@Data
public class PostFeed implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String content;

    private Date createdOn;

    private UUID uuid;

    private String username;

    private String tagName;

    private String groupName;

    private String groupTagName;

}

