package com.rogalski.payitforward.model;

public enum IdentityProvider {
    LOCAL, GOOGLE
}
