package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@MappedSuperclass
@Data
public class Content {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private final UUID uuid = UUID.randomUUID();

    private String content;

    private Date createdOn;

    @ManyToOne()
    private User user;

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Content))
            return false;
        Content content = (Content) o;
        return getUuid().equals(content.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
