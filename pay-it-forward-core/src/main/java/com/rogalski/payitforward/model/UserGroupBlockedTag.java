package com.rogalski.payitforward.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
public class UserGroupBlockedTag {

    @EmbeddedId
    private UserGroupBlockedTagId id;

    private UUID uuid = UUID.randomUUID();

    @ManyToOne
    @MapsId("userId")
    private User user;

    @ManyToOne
    @MapsId("tagId")
    private Tag tag;

    @ManyToOne
    @MapsId("groupId")
    private Group group;

    @Embeddable
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UserGroupBlockedTagId implements Serializable {
        private Long userId;
        private Long tagId;
        private Long groupId;
    }

    public UserGroupBlockedTag (User user, Tag tag, Group group) {
        this.id = new UserGroupBlockedTagId(user.getId(), tag.getId(), group.getId());
        this.user = user;
        this.tag = tag;
        this.group = group;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof UserGroupBlockedTag))
            return false;
        UserGroupBlockedTag that = (UserGroupBlockedTag) o;
        return getUuid().equals(that.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
