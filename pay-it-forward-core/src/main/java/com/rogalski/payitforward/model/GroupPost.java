package com.rogalski.payitforward.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class GroupPost extends Content { //todo change database design

    @ManyToOne()
    private GroupTag groupTag;

    //    @OneToMany(
    //            mappedBy = "id",
    //            cascade = CascadeType.ALL,
    //            orphanRemoval = true)
    //    @OrderBy(value = "createdOn DESC")
    //    private Set<GroupPostComment> comments; //todo add to database

    @ManyToOne(optional = false)
    private Group group;
}
