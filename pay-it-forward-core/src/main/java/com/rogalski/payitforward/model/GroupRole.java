package com.rogalski.payitforward.model;

public enum GroupRole {
    ADMIN, MEMBER
}
