package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "group_member")
@Data
public class GroupMember {

    @EmbeddedId
    private GroupMemberId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("groupId")
    private Group group;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    private User user;

    private final UUID uuid = UUID.randomUUID();

    private final Date joinOn = new Date();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "group_member_permission", joinColumns = {
            @JoinColumn(name = "group_id", referencedColumnName = "group_id"),
            @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    }, inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private Set<GroupPermission> groupPermissions;

    @Data
    @Embeddable
    public static class GroupMemberId implements Serializable {
        Long groupId;
        Long userId;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof GroupMember))
            return false;
        GroupMember that = (GroupMember) o;
        return getUuid().equals(that.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
