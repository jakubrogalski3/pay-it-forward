package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
public class TagPermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private UUID uuid = UUID.randomUUID();

    @Enumerated(EnumType.STRING)
    private TagRole tagRole;

    private String description;

}
