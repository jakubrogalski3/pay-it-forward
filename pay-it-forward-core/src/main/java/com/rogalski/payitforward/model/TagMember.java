package com.rogalski.payitforward.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Data
public class TagMember {

    @EmbeddedId
    private TagMemberId id;

    private final UUID uuid = UUID.randomUUID();

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId(value = "tagId")
    private Tag tag;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId(value = "userId")
    private User user;

    private Date joinOn;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tag_member_permission", joinColumns = {
            @JoinColumn(name = "tag_id", referencedColumnName = "tag_id"),
            @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    }, inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private Set<TagPermission> tagPermissions;

    @Data
    @Embeddable
    public static class TagMemberId implements Serializable {
        Long userId;
        Long tagId;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof TagMember))
            return false;
        TagMember tagMember = (TagMember) o;
        return getUuid().equals(tagMember.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
