package com.rogalski.payitforward.service;

import com.rogalski.payitforward.exception.ResourceForbiddenException;
import com.rogalski.payitforward.model.GroupPost;
import com.rogalski.payitforward.model.Post;
import com.rogalski.payitforward.repository.GroupPostRepository;
import com.rogalski.payitforward.repository.PostRepository;
import com.rogalski.payitforward.security.AuthenticationFacade;
import com.rogalski.payitforward.security.UserPrincipal;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
public class DeletePostService {

    private final PostRepository postRepository;
    private final PostService postService;
    private final GroupPostRepository groupPostRepository;

    public DeletePostService (PostRepository postRepository, PostService postService,
                              GroupPostRepository groupPostRepository) {
        this.postRepository = postRepository;
        this.postService = postService;
        this.groupPostRepository = groupPostRepository;
    }

    public void deleteGlobalPost (Long postId) {
        Post post = postService.getPostById(postId);
        Long userId = AuthenticationFacade.getAuthentication().getId();
        if (post.getUser().getId().equals(userId) || AuthenticationFacade.isUserAdmin()) {
            postRepository.delete(post);
        } else {
            throw new ResourceForbiddenException(new Object[]{
                    MessageFormat.format("User with id: {0} cannot delete post with id: {1}", userId, postId)
            });
        }
    }

    public void deleteGroupPost (Long postId) {
        GroupPost groupPost = postService.getGroupPostById(postId);
        UserPrincipal user = AuthenticationFacade.getAuthentication();
        if (groupPost.getUser().getId().equals(user.getId()) || AuthenticationFacade.isUserAdmin() ||
                AuthenticationFacade.isGroupAdmin(groupPost.getGroup().getId())) {
            groupPostRepository.delete(groupPost);
        } else {
            throw new ResourceForbiddenException(new Object[]{
                    MessageFormat.format("User with id: {0} cannot delete post with id: {1}", user.getId(), postId)
            });
        }
    }
}
