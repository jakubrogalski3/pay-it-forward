package com.rogalski.payitforward.service;

import com.rogalski.payitforward.dto.PageResponse;
import com.rogalski.payitforward.dto.mapper.CommentMapper;
import com.rogalski.payitforward.dto.mapper.PostMapper;
import com.rogalski.payitforward.dto.post.PostDTO;
import com.rogalski.payitforward.exception.notfound.GroupMemberNotFoundException;
import com.rogalski.payitforward.exception.notfound.GroupNotFoundException;
import com.rogalski.payitforward.exception.notfound.PostNotFoundException;
import com.rogalski.payitforward.exception.notfound.TagNotFoundException;
import com.rogalski.payitforward.model.GroupMember;
import com.rogalski.payitforward.model.GroupPost;
import com.rogalski.payitforward.repository.GroupMemberRepository;
import com.rogalski.payitforward.repository.GroupPostRepository;
import com.rogalski.payitforward.repository.GroupRepository;
import com.rogalski.payitforward.repository.GroupTagRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
public class GroupPostService {

    private final PostMapper postMapper = PostMapper.INSTANCE;
    private final CommentMapper commentMapper = CommentMapper.INSTANCE;

    private final GroupPostRepository groupPostRepository;
    private final GroupRepository groupRepository;
    private final GroupTagRepository groupTagRepository;
    private final GroupMemberRepository groupMemberRepository;

    public GroupPostService (GroupPostRepository groupPostRepository, GroupRepository groupRepository,
                             GroupTagRepository groupTagRepository, GroupMemberRepository groupMemberRepository) {
        this.groupPostRepository = groupPostRepository;
        this.groupRepository = groupRepository;
        this.groupTagRepository = groupTagRepository;
        this.groupMemberRepository = groupMemberRepository;
    }

    public PageResponse<PostDTO> getPostsFromGroup (Long groupId, Long userId, Long tagId, Pageable pageable) {
        groupRepository.findById(groupId).orElseThrow(
                () -> new GroupNotFoundException(new Object[]{MessageFormat.format("groupId: {0}", groupId)}));
        if (userId != null && tagId != null) {
            return getUserGroupPostsByTag(groupId, userId, tagId, pageable);
        } else if (userId != null) {
            return getUserGroupPosts(groupId, userId, pageable);
        } else if (tagId != null) {
            return getGroupPostByTag(groupId, tagId, pageable);
        }
        return new PageResponse<>(
                groupPostRepository.findByGroupId(groupId, pageable).map(postMapper::groupPostToPostDTO));
    }

    public PostDTO getGroupPostDtoById (Long id) {
        GroupPost groupPost = getGroupPostById(id);
        PostDTO groupPostDTO = postMapper.groupPostToPostDTO(groupPost);
        //        groupPostDTO.setComments(groupPost.getComments().stream().map(commentMapper::groupPostCommentToCommentDTO)
        //                                          .collect(Collectors.toSet())); //todo uncomment after database changes
        return groupPostDTO;
    }

    private PageResponse<PostDTO> getUserGroupPosts (Long groupId, Long userId, Pageable pageable) {
        GroupMember.GroupMemberId groupMemberId = new GroupMember.GroupMemberId();
        groupMemberId.setGroupId(groupId);
        groupMemberId.setUserId(userId);
        getGroupMember(groupMemberId);
        return new PageResponse<>(groupPostRepository.findByGroupIdAndUserId(groupId, userId, pageable)
                                                     .map(postMapper::groupPostToPostDTO));
    }

    private PageResponse<PostDTO> getUserGroupPostsByTag (Long groupId, Long userId, Long tagId, Pageable pageable) {
        GroupMember.GroupMemberId groupMemberId = new GroupMember.GroupMemberId();
        groupMemberId.setGroupId(groupId);
        groupMemberId.setUserId(userId);
        getGroupMember(groupMemberId);
        return new PageResponse<>(
                groupPostRepository.findByGroupIdAndGroupTagIdAndUserId(groupId, tagId, userId, pageable)
                                   .map(postMapper::groupPostToPostDTO));
    }

    private GroupMember getGroupMember (GroupMember.GroupMemberId groupMemberId) {
        return groupMemberRepository.findById(groupMemberId)
                                    .orElseThrow(() -> new GroupMemberNotFoundException(new Object[]{
                                            MessageFormat.format("groupId: {0}, memberId: {1}",
                                                    groupMemberId.getGroupId(), groupMemberId.getUserId())
                                    }));
    }

    private PageResponse<PostDTO> getGroupPostByTag (Long groupId, Long tagId, Pageable pageable) {
        groupTagRepository.findById(tagId).orElseThrow(() -> new TagNotFoundException(new Object[]{
                MessageFormat.format("groupId: {0}, tagId: {1}", groupId, tagId)
        }));
        return new PageResponse<>(groupPostRepository.findByGroupIdAndGroupTagId(groupId, tagId, pageable)
                                                     .map(postMapper::groupPostToPostDTO));
    }

    private GroupPost getGroupPostById (Long postId) {
        return groupPostRepository.findById(postId).orElseThrow(() -> new PostNotFoundException(new Object[]{
                MessageFormat.format("groupPostId: {0}", postId)
        }));
    }

}
