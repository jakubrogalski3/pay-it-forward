package com.rogalski.payitforward.service;

import com.rogalski.payitforward.dto.comment.AddCommentDTO;
import com.rogalski.payitforward.exception.notfound.CommentNotFoundException;
import com.rogalski.payitforward.exception.notfound.PostNotFoundException;
import com.rogalski.payitforward.model.Comment;
import com.rogalski.payitforward.model.Post;
import com.rogalski.payitforward.repository.CommentRepository;
import com.rogalski.payitforward.repository.PostRepository;
import com.rogalski.payitforward.repository.UserRepository;
import com.rogalski.payitforward.security.AuthenticationFacade;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Date;

@Service
public class SaveCommentService {

    private final CommentRepository commentRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public SaveCommentService (CommentRepository commentRepository, PostRepository postRepository,
                               UserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    public void saveComment (AddCommentDTO commentDTO) {
        Post post;
        try {
            post = postRepository.getOne(commentDTO.getPostId());
        } catch (Exception e) {
            throw new PostNotFoundException(new Object[]{
                    MessageFormat.format("postId: {0}", commentDTO.getPostId())
            });
        }
        if (commentDTO.getParentId() != null && !commentRepository.existsById(commentDTO.getParentId())) {
            throw new CommentNotFoundException(new Object[]{
                    MessageFormat.format("commentId: {0}", commentDTO.getParentId())
            });
        }
        Comment comment = new Comment();
        comment.setContent(commentDTO.getContent());
        comment.setCreatedOn(new Date());
        comment.setUser(AuthenticationFacade.getUser(userRepository));
        comment.setParentId(commentDTO.getParentId());
        comment.setPost(post);
        comment.setParentId(comment.getParentId());
        commentRepository.save(comment);
    }
}
