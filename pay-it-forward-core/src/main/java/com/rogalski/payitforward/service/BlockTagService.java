package com.rogalski.payitforward.service;

import com.rogalski.payitforward.dto.BlockOrUnblockTagDTO;
import com.rogalski.payitforward.model.*;
import com.rogalski.payitforward.repository.*;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BlockTagService {

    private final TagRepository tagRepository;
    private final GroupRepository groupRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final UserBlockedTagRepository userBlockedTagRepository;
    private final UserGroupBlockedTagRepository userGroupBlockedTagRepository;

    public BlockTagService (TagRepository tagRepository, GroupRepository groupRepository, UserService userService,
                            UserRepository userRepository, UserBlockedTagRepository userBlockedTagRepository,
                            UserGroupBlockedTagRepository userGroupBlockedTagRepository) {
        this.tagRepository = tagRepository;
        this.groupRepository = groupRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.userBlockedTagRepository = userBlockedTagRepository;
        this.userGroupBlockedTagRepository = userGroupBlockedTagRepository;
    }

    public void blockTag (Long userId, Long tagId, BlockOrUnblockTagDTO blockTagDTO) {
        User user = userService.getUserById(userId);
        Tag tag = tagRepository
                .findById(tagId)
                .orElseThrow(() -> new RuntimeException("Tag with id: " + tagId + " not found"));
        if (blockTagDTO.isGlobally()) {
            user.addBlockedTag(new UserBlockedTag(user, tag));
        }
        if (blockTagDTO.isForAllGroups()) {
            user
                    .getGroups()
                    .forEach(group -> user.addGroupBlockedTag(new UserGroupBlockedTag(user, tag, group.getGroup())));
        } else if (!blockTagDTO
                .getGroupIds()
                .isEmpty()) {
            blockTagDTO
                    .getGroupIds()
                    .forEach(group -> {
                        Optional<Group> groupOptional = groupRepository.findById(group);
                        groupOptional.ifPresent(
                                foundGroup -> user.addGroupBlockedTag(new UserGroupBlockedTag(user, tag, foundGroup)));
                    });
        }
        userRepository.save(user);
    }

    public boolean unblockTag (Long userId, Long tagId, BlockOrUnblockTagDTO unblockTagDTO) {
        //        User user = userService.getUserById(userId);
        //        if (unblockTagDTO.isGlobally()) {
        //            Optional<UserBlockedTag> userBlockedTag = userBlockedTagRepository.findById(
        //                    new UserBlockedTag.UserBlockedTagId(userId, tagId));
        //            userBlockedTag.ifPresent(user::removeBlockedTag);
        //        }
        //        if (unblockTagDTO.isForAllGroups()) {
        //            List<UserGroupBlockedTag> userGroupBlockedTags =
        //                    userGroupBlockedTagRepository.findByUserIdAndTagId(userId, tagId);
        //            user.removeGroupBlockedTags(userGroupBlockedTags);
        //        } else if(!unblockTagDTO.getGroupIds().isEmpty()) {
        //            unblockTagDTO.getGroupIds().forEach(group -> {
        //                List<UserGroupBlockedTag> userGroupBlockedTags =
        //                        userGroupBlockedTagRepository
        //                                .findByUserIdAndTagIdAndGroupId(userId, tagId, group);
        //                user.removeGroupBlockedTags(userGroupBlockedTags);
        //            });
        //        }
        //        userRepository.save(user);
        return userBlockedTagRepository.isTagBlockedGloballyByUser(1L, 2L);
    }
}
