package com.rogalski.payitforward.service;

import com.rogalski.payitforward.dto.mapper.CommentMapper;
import com.rogalski.payitforward.dto.post.CommentDTO;
import com.rogalski.payitforward.repository.CommentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CommentService {

    private final CommentMapper commentMapper = CommentMapper.INSTANCE;

    private final CommentRepository commentRepository;

    public CommentService (CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public List<CommentDTO> getCommentsFromPost (Long postId) {
        List<CommentDTO> commentsDTOFlat = commentRepository
                .findAllByPostIdOrderByCreatedOnAsc(postId)
                .stream()
                .map(commentMapper::commentToCommentDTO)
                .collect(Collectors.toList());
        Map<Long, List<CommentDTO>> map = commentsDTOFlat
                .stream()
                .filter(comment -> comment.getParentId() != null)
                .collect(Collectors.groupingBy(CommentDTO::getParentId));
        commentsDTOFlat.forEach(comment -> {
            if (map.containsKey(comment.getId())) {
                comment
                        .getChildren()
                        .addAll(map.get(comment.getId()));
            }
        });
        return commentsDTOFlat
                .stream()
                .filter(comment -> comment.getParentId() == null)
                .collect(Collectors.toList());
    }

}
