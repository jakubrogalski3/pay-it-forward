package com.rogalski.payitforward.service;

import com.rogalski.payitforward.dto.PageResponse;
import com.rogalski.payitforward.dto.mapper.PostFeedMapper;
import com.rogalski.payitforward.dto.mapper.PostMapper;
import com.rogalski.payitforward.dto.post.PostDTO;
import com.rogalski.payitforward.dto.post.PostFeedDTO;
import com.rogalski.payitforward.exception.notfound.PostNotFoundException;
import com.rogalski.payitforward.model.GroupPost;
import com.rogalski.payitforward.model.Post;
import com.rogalski.payitforward.model.PostFeed;
import com.rogalski.payitforward.repository.GroupPostRepository;
import com.rogalski.payitforward.repository.PostFeedRepository;
import com.rogalski.payitforward.repository.PostRepository;
import com.rogalski.payitforward.security.AuthenticationFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
public class PostService {

    private final static PostMapper postMapper = PostMapper.INSTANCE;
    private final static PostFeedMapper postFeedMapper = PostFeedMapper.INSTANCE;

    private final PostRepository postRepository;
    private final GroupPostRepository groupPostRepository;
    private final PostFeedRepository postFeedRepository;
    private final CommentService commentService;

    public PostService (PostRepository postRepository, GroupPostRepository groupPostRepository,
                        PostFeedRepository postFeedRepository, CommentService commentService) {
        this.postRepository = postRepository;
        this.groupPostRepository = groupPostRepository;
        this.postFeedRepository = postFeedRepository;
        this.commentService = commentService;
    }

    public PageResponse<PostDTO> getPostsByTag (Long tagId, Pageable pageable) {
        return new PageResponse<>(postRepository
                .findByTagId(tagId, pageable)
                .map(postMapper::postToPostDTO));
    }

    public PageResponse<PostFeedDTO> getUserGlobalPosts (Long userId, Pageable pageable) {
        if (AuthenticationFacade.isAnonymous()) {
            return new PageResponse<>(postRepository
                    .findByUserId(userId, pageable)
                    .map(postFeedMapper::postToPostFeedDTO));
        } else {
            Long loggedUserId = AuthenticationFacade
                    .getAuthentication()
                    .getId();
            return new PageResponse<>(postFeedRepository
                    .findUserPosts(userId, loggedUserId, pageable)
                    .map(postFeedMapper::postFeedToPostFeedDTO));
        }
    }

    public PageResponse<PostDTO> getUserTagPosts (Long tagId, Long userId, Pageable pageable) {
        return new PageResponse<>(postRepository
                .findPostsByTagIdAndUserId(tagId, userId, pageable)
                .map(postMapper::postToPostDTO));
    }

    public PageResponse<PostFeedDTO> getPostFeedForUser (Pageable pageable) {
        Long userId = AuthenticationFacade
                .getAuthentication()
                .getId();
        Page<PostFeed> postsPage = postFeedRepository.findPostFeedByUserId(userId, pageable);
        Page<PostFeedDTO> postsDTOPage = postsPage.map(postFeedMapper::postFeedToPostFeedDTO);
        return new PageResponse<>(postsDTOPage);
    }

    public PostDTO getPostDtoById (Long postId) {
        Post post = getPostById(postId);
        PostDTO postDTO = postMapper.postToPostDTO(post);
        postDTO.setComments(commentService.getCommentsFromPost(post.getId()));
        return postDTO;
    }

    Post getPostById (Long postId) {
        return postRepository
                .findById(postId)
                .orElseThrow(() -> new PostNotFoundException(new Object[]{
                        MessageFormat.format("postId: {0}", postId)
                }));
    }

    GroupPost getGroupPostById (Long postId) {
        return groupPostRepository
                .findById(postId)
                .orElseThrow(() -> new PostNotFoundException(new Object[]{
                        MessageFormat.format("groupPostId: {0}", postId)
                }));
    }

}
