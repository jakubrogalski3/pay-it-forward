package com.rogalski.payitforward.service;

import com.rogalski.payitforward.dto.mapper.PostMapper;
import com.rogalski.payitforward.dto.post.*;
import com.rogalski.payitforward.exception.ResourceForbiddenException;
import com.rogalski.payitforward.exception.ValidationException;
import com.rogalski.payitforward.exception.notfound.PostNotFoundException;
import com.rogalski.payitforward.exception.notfound.TagNotFoundException;
import com.rogalski.payitforward.model.*;
import com.rogalski.payitforward.repository.*;
import com.rogalski.payitforward.security.AuthenticationFacade;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Map;

@Service
public class SavePostService {

    private final PostMapper postMapper = PostMapper.INSTANCE;

    private final PostRepository postRepository;
    private final GroupPostRepository groupPostRepository;
    private final UserRepository userRepository;
    private final TagRepository tagRepository;
    private final GroupTagRepository groupTagRepository;
    private final GroupRepository groupRepository;

    public SavePostService (PostRepository postRepository, GroupPostRepository groupPostRepository,
                            UserRepository userRepository, TagRepository tagRepository,
                            GroupTagRepository groupTagRepository, GroupRepository groupRepository) {
        this.postRepository = postRepository;
        this.groupPostRepository = groupPostRepository;
        this.userRepository = userRepository;
        this.tagRepository = tagRepository;
        this.groupTagRepository = groupTagRepository;
        this.groupRepository = groupRepository;
    }

    public PostDTO createGlobalPost (CreateGlobalPostDTO postDto) {
        User user = AuthenticationFacade.getUser(userRepository);
        Tag tag = tagRepository.findById(postDto.getTagId()).orElseThrow(
                () -> new ValidationException("Wrong tagId. Tag with id " + postDto.getTagId() + " not found",
                        Map.of("tagId", "Tag not found")));

        return saveGlobalPost(postDto, tag, user);
    }

    public PostDTO createGroupPost (Long groupId, CreateGroupPostDTO postDTO) {
        User user = AuthenticationFacade.getUser(userRepository);
        Group group = groupRepository.findById(groupId).orElseThrow(
                () -> new ValidationException("Wrong groupId. Group with id " + groupId + " not found",
                        Map.of("groupId", "Group not found")));
        //        if (group.getConfiguration) todo add group configuration (e.g. if tag is always required)
        GroupTag tag = null;
        if (postDTO.getTagId() != null) {
            tag = groupTagRepository.findById(postDTO.getTagId()).orElseThrow(
                    () -> new ValidationException("Wrong tagId. Tag with id " + postDTO.getTagId() + " not found",
                            Map.of("tagId", "Tag not found")));
        }
        return saveGroupPost(postDTO, group, tag, user);
    }

    private PostDTO saveGlobalPost (CreatePostDTO postDto, Tag tag, User user) {
        Post post = new Post();
        post.setCreatedOn(new Date());
        post.setUser(user);
        post.setContent(postDto.getContent());
        post.setTag(tag);

        return postMapper.postToPostDTO(postRepository.save(post));
    }

    private PostDTO saveGroupPost (CreateGroupPostDTO postDto, Group group, GroupTag groupTag, User user) {
        GroupPost groupPost = new GroupPost();
        groupPost.setCreatedOn(new Date());
        groupPost.setUser(user);
        groupPost.setContent(postDto.getContent());
        groupPost.setGroup(group);

        if (groupTag != null) {
            groupPost.setGroupTag(groupTag);
        }
        return postMapper.groupPostToPostDTO(groupPostRepository.save(groupPost));
    }

    public PostDTO updatePost (Long postId, UpdatePostDTO postDTO) {
        Post post = postRepository.findById(postId).orElseThrow(
                () -> new PostNotFoundException(new Object[]{MessageFormat.format("postId: {0}", postId)}));
        if (AuthenticationFacade.isUserAdmin() ||
                post.getUser().getId().equals(AuthenticationFacade.getAuthentication().getId())) {
            if (StringUtils.hasText(postDTO.getContent())) {
                post.setContent(postDTO.getContent());
            }
            if (postDTO.getTagId() != null) {
                Tag tag = tagRepository.findById(postDTO.getTagId()).orElseThrow(() -> new TagNotFoundException(
                        new Object[]{MessageFormat.format("tagId: {0}", postDTO.getTagId())}));
                post.setTag(tag);
            }
            postRepository.save(post);
            return postMapper.postToPostDTO(post);
        }
        throw new ResourceForbiddenException(new Object[]{
                MessageFormat.format("User with id: {0} cannot update post with id: {1}",
                        AuthenticationFacade.getAuthentication().getId(), postId)
        });
    }

    public PostDTO updateGroupPost (Long postId, UpdateGroupPostDTO updatePostDTO) {
        GroupPost groupPost = groupPostRepository.findById(postId).orElseThrow(
                () -> new PostNotFoundException(new Object[]{MessageFormat.format("groupPostId: {0}", postId)}));
        if (AuthenticationFacade.isUserAdmin() || AuthenticationFacade.isGroupAdmin(groupPost.getGroup().getId()) ||
                groupPost.getUser().getId().equals(AuthenticationFacade.getAuthentication().getId())) {
            if (StringUtils.hasText(updatePostDTO.getContent())) {
                groupPost.setContent(updatePostDTO.getContent());
            }
            if (updatePostDTO.getTagId() != null) {
                GroupTag groupTag = groupTagRepository.findById(updatePostDTO.getTagId()).orElseThrow(
                        () -> new TagNotFoundException(
                                new Object[]{MessageFormat.format("groupTagId: {0}", updatePostDTO.getTagId())}));
                groupPost.setGroupTag(groupTag);
            }
            groupPostRepository.save(groupPost); //todo add edited on + edited by hibernate audit logs?
            return postMapper.groupPostToPostDTO(groupPost);
        }
        throw new ResourceForbiddenException(new Object[]{
                MessageFormat.format("User with id: {0} cannot update group post with id: {1}",
                        AuthenticationFacade.getAuthentication().getId(), postId)
        });
    }
}
