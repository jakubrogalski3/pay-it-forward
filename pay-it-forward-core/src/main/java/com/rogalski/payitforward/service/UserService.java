package com.rogalski.payitforward.service;

import com.rogalski.payitforward.model.User;
import com.rogalski.payitforward.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserById (Long userId) {
        return userRepository.findById(userId)
                             .orElseThrow(() -> new RuntimeException("User with id: " + userId + " not found"));
    }
}
