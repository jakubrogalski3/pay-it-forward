package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserSecurityRepository extends JpaRepository<User, Long> {

    //    @Query("SELECT DISTINCT u FROM User u " +
    //            "JOIN FETCH u.permission p " +
    //            "LEFT JOIN FETCH u.groups g " +
    //            "LEFT JOIN FETCH g.groupPermissions gp " +
    //            "LEFT JOIN FETCH u.tags t " +
    //            "LEFT JOIN FETCH t.tagPermissions tp " +
    //            "WHERE u.username = :username")

    //    @EntityGraph(value = "graph.UserSystemPermission")
    Optional<User> findByUsername (String username);

    //    @Query("SELECT DISTINCT u FROM User u " +
    //            "JOIN FETCH u.permission p " +
    //            "LEFT JOIN FETCH u.groups g " +
    //            "LEFT JOIN FETCH g.groupPermissions gp " +
    //            "LEFT JOIN FETCH u.tags t " +
    //            "LEFT JOIN FETCH t.tagPermissions tp " +
    //            "WHERE u.id = :id")

    //    @EntityGraph(value = "graph.UserSystemPermission")
    Optional<User> findById (Long id);

    //    private final EntityManager entityManager;
    //
    //    public UserSecurityRepository(EntityManagerFactory emf) {
    //        entityManager = emf.createEntityManager();
    //    }
    //
    //    public Optional<User> findByUsernameWithPermissions (String username) {
    //        EntityGraph<?> systemPermissionsGraph = entityManager.createEntityGraph("graph.UserSystemPermission");
    //        EntityGraph<?> groupPermissionGraph = entityManager.createEntityGraph("graph.UserGroupPermissions");
    //        EntityGraph<?> tagPermissionGraph = entityManager.createEntityGraph("graph.UserTagPermissions");
    //        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.username = :username", User.class);
    //        query.setParameter("username", username);
    //        query.setHint("javax.persistence.fetchgraph", Arrays.asList(systemPermissionsGraph, groupPermissionGraph, tagPermissionGraph));
    ////        query.setHint("javax.persistence.fetchgraph", groupPermissionGraph);
    ////        query.setHint("javax.persistence.fetchgraph", tagPermissionGraph);
    //        try {
    //            return Optional.of(query.getSingleResult());
    //        } catch (Exception e) {
    //            return Optional.empty();
    //        }
    //    }
    //
    //    public Optional<User> findByIdWithPermissions (Long id) {
    //        EntityGraph<?> systemPermissionsGraph = entityManager.createEntityGraph("graph.UserSystemPermission");
    //        EntityGraph<?> groupPermissionGraph = entityManager.createEntityGraph("graph.UserGroupPermissions");
    //        EntityGraph<?> tagPermissionGraph = entityManager.createEntityGraph("graph.UserTagPermissions");
    //        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.id = :userId", User.class);
    //        query.setParameter("userId", id);
    //        query.setHint("javax.persistence.fetchgraph", Arrays.asList(systemPermissionsGraph, groupPermissionGraph, tagPermissionGraph));
    ////        query.setHint("javax.persistence.fetchgraph", groupPermissionGraph);
    ////        query.setHint("javax.persistence.fetchgraph", tagPermissionGraph);
    //        try {
    //            return Optional.of(query.getSingleResult());
    //        } catch (Exception e) {
    //            return Optional.empty();
    //        }
    //    }
}
