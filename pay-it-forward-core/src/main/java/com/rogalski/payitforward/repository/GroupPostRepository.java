package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.GroupPost;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupPostRepository extends JpaRepository<GroupPost, Long> {
    Page<GroupPost> findByGroupId (Long groupId, Pageable pageable);

    Page<GroupPost> findByGroupIdAndUserId (Long groupId, Long userId, Pageable pageable);

    Page<GroupPost> findByGroupIdAndGroupTagId (Long groupId, Long tagId, Pageable pageable);

    Page<GroupPost> findByGroupIdAndGroupTagIdAndUserId (Long groupId, Long tagId, Long userId, Pageable pageable);
}
