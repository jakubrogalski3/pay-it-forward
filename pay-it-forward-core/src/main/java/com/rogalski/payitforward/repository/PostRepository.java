package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {

    Page<Post> findByTagId (Long tagId, Pageable pageRequest);

    //    Page<Post> findByGroupId(Long id, Pageable pageable);
    Page<Post> findByUserId (Long id, Pageable pageable);

    //    @Query("SELECT p FROM Post p WHERE p.group.id = ?1 AND p.user.id = ?2")
    //    Page<Post> findGroupPostsByUserId(Long groupId, Long userId, Pageable pageable);
    Page<Post> findPostsByTagIdAndUserId (Long tagId, Long userId, Pageable pageable);

    //    @Query("SELECT p FROM Post p " +
    //            "LEFT OUTER JOIN GroupMember gm ON p.group.id = gm.id.groupId " +
    //            "LEFT OUTER JOIN TagMember tm ON p.tag.id = tm.id.tagId " +
    //            "WHERE gm.id.userId = ?1 OR tm.id.userId = ?1 " +
    //            "ORDER BY p.createdOn DESC")
    //    Page<Post> findPostForUserFeed(Long userId, Pageable pageable);

}
