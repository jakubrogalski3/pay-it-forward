package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.UserBlockedTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserBlockedTagRepository extends JpaRepository<UserBlockedTag, UserBlockedTag.UserBlockedTagId> {

    @Query("SELECT case WHEN (COUNT(1) > 0) THEN true ELSE false END " +
            "FROM UserBlockedTag WHERE user_id = ?1 AND tag_id = ?2")
    Boolean isTagBlockedGloballyByUser (Long userId, Long tagId);

}
