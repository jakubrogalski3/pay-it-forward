package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByPostIdOrderByCreatedOnAsc (Long postId);
}
