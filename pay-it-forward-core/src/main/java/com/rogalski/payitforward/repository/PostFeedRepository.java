package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.PostFeed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.StoredProcedureQuery;

@Repository
public class PostFeedRepository {

    private final EntityManager entityManager;

    public PostFeedRepository (EntityManagerFactory emf) {
        entityManager = emf.createEntityManager();
    }

    public Page<PostFeed> findPostFeedByUserId (Long userId, Pageable pageable) {
        StoredProcedureQuery postFeedListQuery = entityManager.createNamedStoredProcedureQuery("findPostFeedByUserId");
        postFeedListQuery.setParameter("userId", userId);
        postFeedListQuery.setParameter("limit", pageable.getPageSize());
        postFeedListQuery.setParameter("offset", pageable.getOffset());

        StoredProcedureQuery postFeedCountQuery =
                entityManager.createNamedStoredProcedureQuery("findPostFeedCountByUserId");
        postFeedCountQuery.setParameter("userId", userId);

        return new PageImpl<PostFeed>(postFeedListQuery.getResultList(), pageable,
                (int) postFeedCountQuery.getSingleResult());
    }

    public Page<PostFeed> findUserPosts (Long userId, Long viewerId, Pageable pageable) {
        StoredProcedureQuery postFeedListQuery = entityManager.createNamedStoredProcedureQuery("findUserPosts");
        postFeedListQuery.setParameter("userId", userId);
        postFeedListQuery.setParameter("viewerId", viewerId);
        postFeedListQuery.setParameter("limit", pageable.getPageSize());
        postFeedListQuery.setParameter("offset", pageable.getOffset());

        StoredProcedureQuery postFeedCountQuery = entityManager.createNamedStoredProcedureQuery("findUserPostsCount");
        postFeedCountQuery.setParameter("userId", userId);
        postFeedCountQuery.setParameter("viewerId", viewerId);

        return new PageImpl<PostFeed>(postFeedListQuery.getResultList(), pageable,
                (int) postFeedCountQuery.getSingleResult());
    }
}
