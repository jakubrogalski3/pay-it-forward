package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.GroupPostComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupPostCommentRepository extends JpaRepository<GroupPostComment, Long> {
}
