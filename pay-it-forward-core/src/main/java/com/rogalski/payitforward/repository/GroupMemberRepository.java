package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.GroupMember;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupMemberRepository extends JpaRepository<GroupMember, GroupMember.GroupMemberId> {

}
