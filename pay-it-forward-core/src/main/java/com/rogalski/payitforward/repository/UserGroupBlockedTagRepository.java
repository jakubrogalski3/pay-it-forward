package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.Group;
import com.rogalski.payitforward.model.UserGroupBlockedTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserGroupBlockedTagRepository
        extends JpaRepository<UserGroupBlockedTag, UserGroupBlockedTag.UserGroupBlockedTagId> {
    List<UserGroupBlockedTag> findByUserIdAndTagId (Long userId, Long tagId);

    List<UserGroupBlockedTag> findByUserIdAndTagIdAndGroupId (Long userId, Long tagId, Long groupId);

    @Query("SELECT g FROM Group g " + "LEFT OUTER JOIN UserGroupBlockedTag ugbt ON g.id = ugbt.id.groupId " +
            "WHERE ugbt.id.userId = ?1 AND ugbt.id.tagId = ?2")
    List<Group> findGroupsInUserGroupBlockedTags (Long userId, Long tagId);

}
