package com.rogalski.payitforward.repository;

import com.rogalski.payitforward.model.GroupTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupTagRepository extends JpaRepository<GroupTag, Long> {
}
