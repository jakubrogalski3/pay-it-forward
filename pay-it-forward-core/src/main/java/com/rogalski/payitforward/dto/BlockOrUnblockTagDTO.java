package com.rogalski.payitforward.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BlockOrUnblockTagDTO {

    private boolean globally;

    private boolean forAllGroups;

    private List<Long> groupIds = new ArrayList<>();

}
