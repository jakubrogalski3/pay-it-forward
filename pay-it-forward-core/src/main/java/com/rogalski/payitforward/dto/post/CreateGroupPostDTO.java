package com.rogalski.payitforward.dto.post;

import lombok.Data;

@Data
public class CreateGroupPostDTO extends CreatePostDTO {

    private Long tagId;

}
