package com.rogalski.payitforward.dto.post;

import lombok.Data;

@Data
public class GroupPostBasicInfoDTO {

    private Long id;
    private String content;

}
