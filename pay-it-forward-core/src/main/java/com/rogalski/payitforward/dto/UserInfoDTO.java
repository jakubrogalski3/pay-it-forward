package com.rogalski.payitforward.dto;

import lombok.Builder;
import lombok.Data;

@Data
public class UserInfoDTO {

    private String email;

    private String username;

    private String firstName;

    private String lastName;

    @Builder
    public UserInfoDTO (String firstName, String lastName, String username, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
    }
}
