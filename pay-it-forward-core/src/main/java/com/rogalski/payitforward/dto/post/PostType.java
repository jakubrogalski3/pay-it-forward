package com.rogalski.payitforward.dto.post;

import lombok.Getter;

public enum PostType {
    GLOBAL("Global"), GROUP("Group");

    @Getter
    private final String display;

    PostType (String display) {
        this.display = display;
    }
}
