package com.rogalski.payitforward.dto.mapper;

import com.rogalski.payitforward.dto.post.PostDTO;
import com.rogalski.payitforward.model.GroupPost;
import com.rogalski.payitforward.model.Post;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PostMapper {

    PostMapper INSTANCE = Mappers.getMapper(PostMapper.class);

    @Mapping(target = "tag", source = "tag.name")
    @Mapping(target = "username", source = "user.username")
    PostDTO postToPostDTO (Post post);

    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "tag", source = "tag.name")
    @Mapping(target = "username", source = "user.username")
    PostDTO postToPostDTOInList (Post post);

    @Mapping(target = "tag", source = "groupTag.name")
    @Mapping(target = "username", source = "user.username")
    @Mapping(target = "group", source = "group.name")
    PostDTO groupPostToPostDTO (GroupPost post);

}
