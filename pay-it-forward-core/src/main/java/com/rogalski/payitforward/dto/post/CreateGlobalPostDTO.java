package com.rogalski.payitforward.dto.post;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class CreateGlobalPostDTO extends CreatePostDTO {

    @NotNull
    @Min(value = 1)
    private Long tagId;

    public CreateGlobalPostDTO (
            @NotEmpty @Size(min = 11, message = "Post content must be more than 10 characters") String content,
            @NotNull @Min(value = 1) Long tagId) {
        super(content);
        this.tagId = tagId;
    }
}
