package com.rogalski.payitforward.dto.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePostDTO {

    @NotEmpty
    @Size(min = 11, message = "Post content must be more than 10 characters")
    private String content;

}
