package com.rogalski.payitforward.dto.mapper;

import com.rogalski.payitforward.dto.TagDTO;
import com.rogalski.payitforward.model.Tag;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TagMapper {

    TagMapper INSTANCE = Mappers.getMapper(TagMapper.class);

    TagDTO tagToTagDTO (Tag tag);
}
