package com.rogalski.payitforward.dto.group;

import com.rogalski.payitforward.dto.UserBasicInfoDTO;
import com.rogalski.payitforward.dto.post.GroupPostBasicInfoDTO;
import lombok.Data;

import java.util.List;

@Data
public class GroupDetailsDTO {

    private Long id;
    private String name;
    private List<GroupPostBasicInfoDTO> posts;
    private List<UserBasicInfoDTO> members;

}
