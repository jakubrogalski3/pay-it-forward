package com.rogalski.payitforward.dto;

import lombok.Builder;
import lombok.Data;

@Data
public class UserRegisterDTO {

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    @Builder
    public UserRegisterDTO (String firstName, String lastName, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
    }
}
