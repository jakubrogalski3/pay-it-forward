package com.rogalski.payitforward.dto.post;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class CommentDTO {

    private Long id;

    private String username;

    private String content;

    private Date createdOn;

    private Long parentId;

    private List<CommentDTO> children = new ArrayList<>();
}
