package com.rogalski.payitforward.dto.comment;

import com.rogalski.payitforward.dto.post.CreatePostDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddCommentDTO extends CreatePostDTO {

    @NotNull
    @Min(value = 1)
    private Long postId;
    private Long parentId;

}
