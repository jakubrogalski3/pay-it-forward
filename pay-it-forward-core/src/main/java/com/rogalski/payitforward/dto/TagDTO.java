package com.rogalski.payitforward.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TagDTO {

    private String name;

    private String description;

    private Date createdOn;

    private String image;
}
