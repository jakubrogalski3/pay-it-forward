package com.rogalski.payitforward.dto.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {

    private Long id;

    private String username;

    private String content;

    private String tag;

    private String group;

    private Long groupId;

    private List<CommentDTO> comments = new ArrayList<>();

}
