package com.rogalski.payitforward.dto;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
public class PageResponse<T> {

    private List<T> content;
    private Long total;
    private Integer number;
    private Integer size;

    public PageResponse (Page<T> page) {
        this.content = page.getContent();
        this.total = page.getTotalElements();
        this.number = page.getNumber();
        this.size = page.getSize();
    }

}
