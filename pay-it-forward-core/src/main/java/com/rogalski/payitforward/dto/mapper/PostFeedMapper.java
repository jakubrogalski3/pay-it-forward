package com.rogalski.payitforward.dto.mapper;

import com.rogalski.payitforward.dto.post.PostFeedDTO;
import com.rogalski.payitforward.dto.post.PostType;
import com.rogalski.payitforward.model.Post;
import com.rogalski.payitforward.model.PostFeed;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import org.springframework.util.StringUtils;

@Mapper
public interface PostFeedMapper {

    PostFeedMapper INSTANCE = Mappers.getMapper(PostFeedMapper.class);

    @Mapping(target = "tag", source = "tagName")
    @Mapping(target = "group", source = "groupName")
    PostFeedDTO postFeedToPostFeedDTO (PostFeed postFeed);

    @Mapping(target = "tag", source = "tag.name")
    PostFeedDTO postToPostFeedDTO (Post post);

    @AfterMapping
    default void setPostType (@MappingTarget PostFeedDTO dto) {
        if (StringUtils.hasText(dto.getGroup())) {
            dto.setPostType(PostType.GROUP.getDisplay());
        } else {
            dto.setPostType(PostType.GLOBAL.getDisplay());
        }
    }

}
