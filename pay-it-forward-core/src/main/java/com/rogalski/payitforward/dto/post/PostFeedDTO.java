package com.rogalski.payitforward.dto.post;

import lombok.Data;

@Data
public class PostFeedDTO extends PostDTO {

    private String postType;

}
