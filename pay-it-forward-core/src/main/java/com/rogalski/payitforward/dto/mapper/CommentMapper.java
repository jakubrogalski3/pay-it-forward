package com.rogalski.payitforward.dto.mapper;

import com.rogalski.payitforward.dto.post.CommentDTO;
import com.rogalski.payitforward.model.Comment;
import com.rogalski.payitforward.model.GroupPostComment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {

    CommentMapper INSTANCE = Mappers.getMapper(CommentMapper.class);

    @Mapping(target = "username", source = "user.username")
    CommentDTO commentToCommentDTO (Comment comment);

    CommentDTO groupPostCommentToCommentDTO (GroupPostComment comment);
}
