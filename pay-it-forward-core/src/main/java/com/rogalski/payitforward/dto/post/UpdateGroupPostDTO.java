package com.rogalski.payitforward.dto.post;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Data
public class UpdateGroupPostDTO {

    @Size(min = 11, message = "Post content must be more than 10 characters")
    private String content;

    @Min(value = 1)
    private Long tagId;
}
