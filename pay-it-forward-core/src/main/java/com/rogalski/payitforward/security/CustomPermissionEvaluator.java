package com.rogalski.payitforward.security;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

public class CustomPermissionEvaluator implements PermissionEvaluator {

    @Override
    public boolean hasPermission (Authentication auth, Object permissionGroup, Object permission) {
        if ((auth == null) || (permissionGroup == null) || !(permission instanceof String) ||
                permission.toString().isEmpty()) {
            return false;
        }

        return hasSystemPrivilege(auth, permissionGroup.toString(), permission.toString().toUpperCase());
    }

    @Override
    public boolean hasPermission (Authentication authentication, Serializable targetId, String targetType,
                                  Object permission) {
        if ((authentication == null) || targetId == null || (targetType == null) || !(permission instanceof String) ||
                permission.toString().isEmpty()) {
            return false;
        }

        return hasSystemPrivilege(authentication, "SYSTEM", "ADMIN") ||
                hasPrivilege(authentication, targetType, targetId.toString(), permission.toString().toUpperCase());
    }

    private boolean hasPrivilege (Authentication auth, String targetType, Object targetId, String permission) {
        String authority = targetType + "_" + targetId + "_" + permission;
        for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
            if (grantedAuth.getAuthority().equals("ADMIN") || grantedAuth.getAuthority().equals(authority)) {
                return true;
            }
        }
        return false;
    }

    private boolean hasSystemPrivilege (Authentication auth, String permissionGroup, String permission) {
        String authority = permissionGroup + "_" + permission;
        for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
            if (grantedAuth.getAuthority().equals(authority)) {
                return true;
            }
        }
        return false;
    }
}
