package com.rogalski.payitforward.security;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AuthenticationFilter extends OncePerRequestFilter {

    @AllArgsConstructor
    public class Route {
        private String url;
        private HttpMethod method;
    }

    private final List<Route> openRoutes = List.of(new Route("/posts/users/", HttpMethod.GET));

    private final static String ID_HEADER = "user_id";
    private final static String ROLES_HEADER = "roles";

    private final CustomUserDetailsService customUserDetailsService;

    public AuthenticationFilter (CustomUserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
    }

    @Override
    protected void doFilterInternal (HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String userIdHeader = request.getHeader(ID_HEADER);
        String userRolesHeader = request.getHeader(ROLES_HEADER);
        if (request.getRequestURI().contains("api-docs")) {
            filterChain.doFilter(request, response);
        }
        if (userIdHeader == null || userIdHeader.isEmpty()) {
            response.setStatus(401);
        } else {
            Long userId = Long.valueOf(userIdHeader);
            String roles = userRolesHeader;
            try {
                UserDetails userDetails = customUserDetailsService.loadUserById(userId, roles);
                UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(),
                                userDetails.getAuthorities());

                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                filterChain.doFilter(request, response);
            } catch (RuntimeException e) {
                response.setStatus(403);
            }
        }
    }

    @Override
    protected boolean shouldNotFilter (HttpServletRequest request) throws ServletException {
        return openRoutes.stream().anyMatch(
                route -> request.getRequestURI().replace("/core-service", "").startsWith(route.url) &&
                        request.getMethod().equals(route.method.name()));
    }
}
