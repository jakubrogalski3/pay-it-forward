package com.rogalski.payitforward.security;

import com.rogalski.payitforward.exception.notfound.UserNotFoundException;
import com.rogalski.payitforward.model.User;
import com.rogalski.payitforward.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.MessageFormat;

public class AuthenticationFacade {

    public static UserPrincipal getAuthentication () {
        return (UserPrincipal) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
    }

    public static boolean isAnonymous () {
        return SecurityContextHolder
                .getContext()
                .getAuthentication() == null;
    }

    public static boolean isUserAdmin () {
        UserPrincipal user = (UserPrincipal) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        return user
                .getAuthorities()
                .stream()
                .anyMatch(authority -> authority
                        .getAuthority()
                        .equals("SYSTEM_ADMIN"));
    }

    public static boolean isGroupAdmin (
            Long groupId) {//SYSTEM_AD MIN,GROUP_1_MEMBER,GROUP_1_ADMIN,TAG_2_ADMIN,TAG_1_ADMIN
        UserPrincipal user = (UserPrincipal) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        return user
                .getAuthorities()
                .stream()
                .anyMatch(authority -> authority
                        .getAuthority()
                        .equals("GROUP_" + groupId + "_ADMIN"));
    }


    public static User getUser (UserRepository userRepository) {
        return userRepository
                .findById(getAuthentication().getId())
                .orElseThrow(() -> new UserNotFoundException(new Object[]{
                        MessageFormat.format("userId: {0}", getAuthentication().getId())
                }));
    }
}
