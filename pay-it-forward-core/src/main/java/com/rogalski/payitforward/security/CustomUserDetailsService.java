package com.rogalski.payitforward.security;

import com.rogalski.payitforward.model.User;
import com.rogalski.payitforward.repository.UserSecurityRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserSecurityRepository userRepository;

    public CustomUserDetailsService (UserSecurityRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername (String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s).orElseThrow(() -> new RuntimeException("User not found"));
        return UserPrincipal.createUser(user); //todo check if it is called
    }

    UserDetails loadUserById (Long id, String roles) {
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        return UserPrincipal.createUser(user, roles);
    }

}
