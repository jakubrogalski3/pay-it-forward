package com.rogalski.payitforward.utils;

import com.rogalski.payitforward.security.AuthenticationFacade;

public class GroupPermissionUtils {

    public static boolean isUserMemberOfGroup (Long groupId) {
        return AuthenticationFacade.getAuthentication().getAuthorities().stream().anyMatch(
                authority -> authority.getAuthority().equals("GROUP_" + groupId + "_PERMISSION_MEMBER") ||
                        authority.getAuthority().equals("GROUP_" + groupId + "_PERMISSION_ADMIN"));
    }
}
