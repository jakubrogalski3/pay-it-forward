package com.rogalski.payitforward.exception.notfound;

import lombok.Data;

@Data
public class ResourceNotFoundException extends RuntimeException {

    private final String messageName;

    private final Object[] args;

    public ResourceNotFoundException (String messageName, Object[] args) {
        super(messageName);
        this.args = args;
        this.messageName = messageName;
    }

}
