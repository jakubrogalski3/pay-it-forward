package com.rogalski.payitforward.exception;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Map;

@Data
public class ValidationErrorDto extends ErrorDto {

    private Map<String, String> fieldsErrors;

    public ValidationErrorDto (String error, String exception, String message, String path, int status,
                               ZonedDateTime timestamp, Map<String, String> fieldsErrors) {
        super(error, exception, message, path, status, timestamp);
        this.fieldsErrors = fieldsErrors;
    }
}
