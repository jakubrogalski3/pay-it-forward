package com.rogalski.payitforward.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
abstract class ErrorUtils {

    static ResponseEntity<ErrorDto> response (Throwable ex, String message, Object[] args, HttpServletRequest req,
                                              HttpStatus status) {
        Object forwardRequestUri = req.getAttribute("javax.servlet.forward.request_uri");
        String path = forwardRequestUri != null ? forwardRequestUri.toString() : req.getRequestURI();
        log.error("{} - {}", message, args, ex);
        ErrorDto error = new ErrorDto(status.getReasonPhrase(), ex
                .getClass()
                .getSimpleName(), message, path, status.value(), ZonedDateTime.now());
        return ResponseEntity
                .status(status.value())
                .body(error);
    }

    static ResponseEntity<ValidationErrorDto> validationResponse (MethodArgumentNotValidException ex, String message,
                                                                  HttpServletRequest req, HttpStatus status) {
        Object forwardRequestUri = req.getAttribute("javax.servlet.forward.request_uri");
        String path = forwardRequestUri != null ? forwardRequestUri.toString() : req.getRequestURI();
        log.info("{}", message, ex);
        Map<String, String> fieldsErrors = new HashMap<>();
        BindingResult bindingResult = ex.getBindingResult();
        bindingResult
                .getFieldErrors()
                .forEach(error -> fieldsErrors.put(error.getField(), error.getDefaultMessage()));
        ValidationErrorDto error = new ValidationErrorDto(status.getReasonPhrase(), ex
                .getClass()
                .getSimpleName(), message, path, status.value(), ZonedDateTime.now(), fieldsErrors);
        return ResponseEntity
                .status(status.value())
                .body(error);
    }

    static ResponseEntity<ValidationErrorDto> validationResponse (ValidationException ex, String message,
                                                                  HttpServletRequest req, HttpStatus status) {
        Object forwardRequestUri = req.getAttribute("javax.servlet.forward.request_uri");
        String path = forwardRequestUri != null ? forwardRequestUri.toString() : req.getRequestURI();
        log.info("{}", message, ex);
        ValidationErrorDto error = new ValidationErrorDto(status.getReasonPhrase(), ex
                .getClass()
                .getSimpleName(), message, path, status.value(), ZonedDateTime.now(), ex.getFieldsErrors());
        return ResponseEntity
                .status(status.value())
                .body(error);
    }
}
