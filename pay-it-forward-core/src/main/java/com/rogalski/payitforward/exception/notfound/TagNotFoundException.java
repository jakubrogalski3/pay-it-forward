package com.rogalski.payitforward.exception.notfound;

public class TagNotFoundException extends ResourceNotFoundException {

    public TagNotFoundException (Object[] args) {
        super("not-found-tag", args);
    }
}
