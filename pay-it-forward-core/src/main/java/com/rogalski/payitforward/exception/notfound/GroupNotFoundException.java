package com.rogalski.payitforward.exception.notfound;

public class GroupNotFoundException extends ResourceNotFoundException {

    public GroupNotFoundException (Object[] args) {
        super("not-found-group", args);
    }
}
