package com.rogalski.payitforward.exception;

import lombok.Data;

@Data
public class ResourceForbiddenException extends RuntimeException {

    private final String messageName;

    private final Object[] args;

    public ResourceForbiddenException (Object[] args) {
        this.messageName = "resource-forbidden";
        this.args = args;
    }
}
