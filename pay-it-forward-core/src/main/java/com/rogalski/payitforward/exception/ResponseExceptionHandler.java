package com.rogalski.payitforward.exception;

import com.rogalski.payitforward.exception.notfound.ResourceNotFoundException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

import static com.rogalski.payitforward.exception.ErrorUtils.response;
import static com.rogalski.payitforward.exception.ErrorUtils.validationResponse;

@ControllerAdvice
public class ResponseExceptionHandler {

    private final MessageSource messageSource;

    public ResponseExceptionHandler (MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    ResponseEntity<ErrorDto> handleNotFoundException (ResourceNotFoundException ex, HttpServletRequest req,
                                                      Locale locale) {
        String messageName = ex.getMessageName();
        Object[] args = ex.getArgs();
        String message = messageSource.getMessage(messageName, args, locale);
        return response(ex, message, ex.getArgs(), req, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ResourceForbiddenException.class)
    ResponseEntity<ErrorDto> handleForbiddenException (ResourceForbiddenException ex, HttpServletRequest req,
                                                       Locale locale) {
        String messageName = ex.getMessageName();
        Object[] args = ex.getArgs();
        String message = messageSource.getMessage(messageName, args, locale);
        return response(ex, message, ex.getArgs(), req, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ValidationErrorDto> handleValidationException (MethodArgumentNotValidException ex,
                                                                  HttpServletRequest req) {
        return validationResponse(ex, ex.getMessage(), req, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidationException.class)
    ResponseEntity<ValidationErrorDto> handleValidationException (ValidationException ex, HttpServletRequest req) {
        return validationResponse(ex, ex.getMessage(), req, HttpStatus.BAD_REQUEST);
    }
}
