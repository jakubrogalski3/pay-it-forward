package com.rogalski.payitforward.exception.notfound;

public class CommentNotFoundException extends ResourceNotFoundException {
    public CommentNotFoundException (Object[] args) {
        super("not-found-comment", args);
    }
}
