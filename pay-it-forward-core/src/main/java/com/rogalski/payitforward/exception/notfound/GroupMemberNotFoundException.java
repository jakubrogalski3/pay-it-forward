package com.rogalski.payitforward.exception.notfound;

public class GroupMemberNotFoundException extends ResourceNotFoundException {

    public GroupMemberNotFoundException (Object[] args) {
        super("not-found-group-member", args);
    }
}
