package com.rogalski.payitforward.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDto {
    /**
     * Error name (e.g. http status code human readable name).
     */
    String error;
    /**
     * Exception that cause error.
     */
    String exception;
    /**
     * Error message.
     */
    String message;
    /**
     * Request path.
     */
    String path;
    /**
     * Response status code.
     */
    int status;
    /**
     * Error timestamp.
     */
    ZonedDateTime timestamp;
}
