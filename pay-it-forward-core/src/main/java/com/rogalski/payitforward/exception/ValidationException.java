package com.rogalski.payitforward.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Map;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
@Data
public class ValidationException extends RuntimeException {

    private Map<String, String> fieldsErrors;

    public ValidationException (String message, Map<String, String> fieldsErrors) {
        super(message);
        this.fieldsErrors = fieldsErrors;
    }
}
