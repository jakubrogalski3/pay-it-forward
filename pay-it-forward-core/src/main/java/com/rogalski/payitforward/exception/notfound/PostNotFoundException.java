package com.rogalski.payitforward.exception.notfound;

public class PostNotFoundException extends ResourceNotFoundException {

    public PostNotFoundException (Object[] args) {
        super("not-found-post", args);
    }
}
