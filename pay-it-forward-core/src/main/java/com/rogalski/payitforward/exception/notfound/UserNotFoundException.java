package com.rogalski.payitforward.exception.notfound;

public class UserNotFoundException extends ResourceNotFoundException {

    public UserNotFoundException (Object[] args) {
        super("not-found-user", args);
    }
}
