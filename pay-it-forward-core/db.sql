Hibernate
:
select post0_.id                as id1_10_0_,
       post0_.content           as content2_10_0_,
       post0_.created_on        as created_3_10_0_,
       post0_.user_id           as user_id5_10_0_,
       post0_.uuid              as uuid4_10_0_,
       post0_.tag_id            as tag_id6_10_0_,
       user1_.id                as id1_9_1_,
       user1_.created_on        as created_2_9_1_,
       user1_.email             as email3_9_1_,
       user1_.first_name        as first_na4_9_1_,
       user1_.identity_provider as identity5_9_1_,
       user1_.is_enabled        as is_enabl6_9_1_,
       user1_.last_name         as last_nam7_9_1_,
       user1_.password          as password8_9_1_,
       user1_.permission_id     as permiss11_9_1_,
       user1_.username          as username9_9_1_,
       user1_.uuid              as uuid10_9_1_,
       groups2_.user_id         as user_id4_1_2_,
       groups2_.group_id        as group_id3_1_2_,
       groups2_.group_id        as group_id3_1_3_,
       groups2_.user_id         as user_id4_1_3_,
       groups2_.join_on         as join_on1_1_3_,
       groups2_.uuid            as uuid2_1_3_,
       grouppermi3_.group_id    as group_id1_2_4_,
       grouppermi3_.user_id     as user_id2_2_4_,
       grouppermi4_.id          as permissi3_2_4_,
       grouppermi4_.id          as id1_3_5_,
       grouppermi4_.description as descript2_3_5_,
       grouppermi4_.group_role  as group_ro3_3_5_,
       grouppermi4_.uuid        as uuid4_3_5_,
       systemperm5_.id          as id1_12_6_,
       systemperm5_.description as descript2_12_6_,
       systemperm5_.permission  as permissi3_12_6_,
       systemperm5_.uuid        as uuid4_12_6_,
       tags6_.user_id           as user_id4_15_7_,
       tags6_.tag_id            as tag_id3_15_7_,
       tags6_.tag_id            as tag_id3_15_8_,
       tags6_.user_id           as user_id4_15_8_,
       tags6_.join_on           as join_on1_15_8_,
       tags6_.uuid              as uuid2_15_8_,
       tagpermiss7_.tag_id      as tag_id1_14_9_,
       tagpermiss7_.user_id     as user_id2_14_9_,
       tagpermiss8_.id          as permissi3_14_9_,
       tagpermiss8_.id          as id1_16_10_,
       tagpermiss8_.description as descript2_16_10_,
       tagpermiss8_.tag_role    as tag_role3_16_10_,
       tagpermiss8_.uuid        as uuid4_16_10_,
       tag9_.id                 as id1_13_11_,
       tag9_.created_on         as created_2_13_11_,
       tag9_.description        as descript3_13_11_,
       tag9_.image              as image4_13_11_,
       tag9_.name               as name5_13_11_,
       tag9_.uuid               as uuid6_13_11_
from post post0_
         left outer join pif_user user1_ on post0_.user_id = user1_.id
         left outer join group_member groups2_ on user1_.id = groups2_.user_id
         left outer join group_member_permission grouppermi3_
                         on groups2_.group_id = grouppermi3_.group_id and groups2_.user_id = grouppermi3_.user_id
         left outer join group_permission grouppermi4_ on grouppermi3_.permission_id = grouppermi4_.id
         left outer join system_permission systemperm5_ on user1_.permission_id = systemperm5_.id
         left outer join tag_member tags6_ on user1_.id = tags6_.user_id
         left outer join tag_member_permission tagpermiss7_
                         on tags6_.tag_id = tagpermiss7_.tag_id and tags6_.user_id = tagpermiss7_.user_id
         left outer join tag_permission tagpermiss8_ on tagpermiss7_.permission_id = tagpermiss8_.id
         inner join tag tag9_ on post0_.tag_id = tag9_.id
where post0_.id = ?
order by groups2_.join_on desc Hibernate:
select comments0_.id            as id1_0_0_,
       comments0_.id            as id1_0_1_,
       comments0_.content       as content2_0_1_,
       comments0_.created_on    as created_3_0_1_,
       comments0_.user_id       as user_id5_0_1_,
       comments0_.uuid          as uuid4_0_1_,
       comments0_.post_id       as post_id6_0_1_,
       user1_.id                as id1_9_2_,
       user1_.created_on        as created_2_9_2_,
       user1_.email             as email3_9_2_,
       user1_.first_name        as first_na4_9_2_,
       user1_.identity_provider as identity5_9_2_,
       user1_.is_enabled        as is_enabl6_9_2_,
       user1_.last_name         as last_nam7_9_2_,
       user1_.password          as password8_9_2_,
       user1_.permission_id     as permiss11_9_2_,
       user1_.username          as username9_9_2_,
       user1_.uuid              as uuid10_9_2_,
       systemperm2_.id          as id1_12_3_,
       systemperm2_.description as descript2_12_3_,
       systemperm2_.permission  as permissi3_12_3_,
       systemperm2_.uuid        as uuid4_12_3_,
       post3_.id                as id1_10_4_,
       post3_.content           as content2_10_4_,
       post3_.created_on        as created_3_10_4_,
       post3_.user_id           as user_id5_10_4_,
       post3_.uuid              as uuid4_10_4_,
       post3_.tag_id            as tag_id6_10_4_,
       user4_.id                as id1_9_5_,
       user4_.created_on        as created_2_9_5_,
       user4_.email             as email3_9_5_,
       user4_.first_name        as first_na4_9_5_,
       user4_.identity_provider as identity5_9_5_,
       user4_.is_enabled        as is_enabl6_9_5_,
       user4_.last_name         as last_nam7_9_5_,
       user4_.password          as password8_9_5_,
       user4_.permission_id     as permiss11_9_5_,
       user4_.username          as username9_9_5_,
       user4_.uuid              as uuid10_9_5_,
       tag5_.id                 as id1_13_6_,
       tag5_.created_on         as created_2_13_6_,
       tag5_.description        as descript3_13_6_,
       tag5_.image              as image4_13_6_,
       tag5_.name               as name5_13_6_,
       tag5_.uuid               as uuid6_13_6_
from comment comments0_
         left outer join pif_user user1_ on comments0_.user_id = user1_.id
         left outer join system_permission systemperm2_ on user1_.permission_id = systemperm2_.id
         left outer join post post3_ on comments0_.post_id = post3_.id
         left outer join pif_user user4_ on post3_.user_id = user4_.id
         left outer join tag tag5_ on post3_.tag_id = tag5_.id
where comments0_.id = ?
order by comments0_.created_on desc

    Hibernate:
select tags0_.user_id as user_id4_15_0_,
       tags0_.tag_id  as tag_id3_15_0_,
       tags0_.tag_id  as tag_id3_15_1_,
       tags0_.user_id as user_id4_15_1_,
       tags0_.join_on as join_on1_15_1_,
       tags0_.uuid    as uuid2_15_1_
from tag_member tags0_
where tags0_.user_id = ?

    Hibernate:
select groups0_.user_id  as user_id4_1_0_,
       groups0_.group_id as group_id3_1_0_,
       groups0_.group_id as group_id3_1_1_,
       groups0_.user_id  as user_id4_1_1_,
       groups0_.join_on  as join_on1_1_1_,
       groups0_.uuid     as uuid2_1_1_
from group_member groups0_
where groups0_.user_id = ?
order by groups0_.join_on desc