Hibernate
:
select user0_.id                  as id1_9_0_,
       tags1_.tag_id              as tag_id3_15_1_,
       tags1_.user_id             as user_id4_15_1_,
       tagpermiss3_.id            as id1_16_2_,
       user0_.created_on          as created_2_9_0_,
       user0_.email               as email3_9_0_,
       user0_.first_name          as first_na4_9_0_,
       user0_.identity_provider   as identity5_9_0_,
       user0_.is_enabled          as is_enabl6_9_0_,
       user0_.last_name           as last_nam7_9_0_,
       user0_.password            as password8_9_0_,
       user0_.permission_id       as permiss11_9_0_,
       user0_.username            as username9_9_0_,
       user0_.uuid                as uuid10_9_0_,
       tags1_.join_on             as join_on1_15_1_,
       tags1_.uuid                as uuid2_15_1_,
       tags1_.user_id             as user_id4_15_0__,
       tags1_.tag_id              as tag_id3_15_0__,
       tagpermiss3_.description   as descript2_16_2_,
       tagpermiss3_.tag_role      as tag_role3_16_2_,
       tagpermiss3_.uuid          as uuid4_16_2_,
       tagpermiss2_.tag_id        as tag_id1_14_1__,
       tagpermiss2_.user_id       as user_id2_14_1__,
       tagpermiss2_.permission_id as permissi3_14_1__
from pif_user user0_
         left outer join tag_member tags1_ on user0_.id = tags1_.user_id
         left outer join tag_member_permission tagpermiss2_
                         on tags1_.tag_id = tagpermiss2_.tag_id and tags1_.user_id = tagpermiss2_.user_id
         left outer join tag_permission tagpermiss3_ on tagpermiss2_.permission_id = tagpermiss3_.id
where user0_.id = ? Hibernate:
select systemperm0_.id          as id1_12_0_,
       systemperm0_.description as descript2_12_0_,
       systemperm0_.permission  as permissi3_12_0_,
       systemperm0_.uuid        as uuid4_12_0_
from system_permission systemperm0_
where systemperm0_.id = ? Hibernate:
select groups0_.user_id  as user_id4_1_0_,
       groups0_.group_id as group_id3_1_0_,
       groups0_.group_id as group_id3_1_1_,
       groups0_.user_id  as user_id4_1_1_,
       groups0_.join_on  as join_on1_1_1_,
       groups0_.uuid     as uuid2_1_1_
from group_member groups0_
where groups0_.user_id = ?
order by groups0_.join_on desc Hibernate:
select grouppermi0_.group_id      as group_id1_2_0_,
       grouppermi0_.user_id       as user_id2_2_0_,
       grouppermi0_.permission_id as permissi3_2_0_,
       grouppermi1_.id            as id1_3_1_,
       grouppermi1_.description   as descript2_3_1_,
       grouppermi1_.group_role    as group_ro3_3_1_,
       grouppermi1_.uuid          as uuid4_3_1_
from group_member_permission grouppermi0_
         inner join group_permission grouppermi1_ on grouppermi0_.permission_id = grouppermi1_.id
where grouppermi0_.group_id = ?
  and grouppermi0_.user_id = ? Hibernate:
select group0_.id         as id1_7_0_,
       group0_.created_on as created_2_7_0_,
       group0_.name       as name3_7_0_,
       group0_.uuid       as uuid4_7_0_
from pif_group group0_
where group0_.id = ? Hibernate:
select members0_.group_id as group_id3_1_0_,
       members0_.user_id  as user_id4_1_0_,
       members0_.group_id as group_id3_1_1_,
       members0_.user_id  as user_id4_1_1_,
       members0_.join_on  as join_on1_1_1_,
       members0_.uuid     as uuid2_1_1_
from group_member members0_
where members0_.group_id = ?