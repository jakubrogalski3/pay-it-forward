create table comment
(
    id         bigserial not null,
    content    varchar(255),
    created_on timestamp,
    uuid       uuid,
    user_id    int8,
    post_id    int8,
    primary key (id)
)
create table group_member
(
    join_on  timestamp,
    uuid     uuid,
    group_id int8 not null,
    user_id  int8 not null,
    primary key (group_id, user_id)
)
create table group_member_permission
(
    group_id      int8 not null,
    user_id       int8 not null,
    permission_id int8 not null,
    primary key (group_id, user_id, permission_id)
)
create table group_permission
(
    id          bigserial not null,
    description varchar(255),
    group_role  varchar(255),
    uuid        uuid,
    primary key (id)
)
create table group_post
(
    id         bigserial not null,
    content    varchar(255),
    created_on timestamp,
    uuid       uuid,
    user_id    int8,
    group_id   int8,
    tag_id     int8,
    primary key (id)
)
create table group_tag
(
    id          bigserial not null,
    created_on  timestamp,
    description varchar(255),
    image       varchar(255),
    name        varchar(255),
    uuid        uuid,
    primary key (id)
)
create table pif_group
(
    id         bigserial not null,
    created_on timestamp,
    name       varchar(255),
    uuid       uuid,
    primary key (id)
)
create table pif_like
(
    id         bigserial not null,
    created_on timestamp,
    uuid       uuid,
    post_id    int8,
    user_id    int8,
    primary key (id)
)
create table pif_user
(
    id                bigserial not null,
    created_on        timestamp,
    email             varchar(255),
    first_name        varchar(255),
    identity_provider varchar(255),
    is_enabled        boolean   not null,
    last_name         varchar(255),
    password          varchar(255),
    username          varchar(255),
    uuid              uuid,
    permission_id     int8,
    primary key (id)
)
create table post
(
    id         bigserial not null,
    content    varchar(255),
    created_on timestamp,
    uuid       uuid,
    user_id    int8,
    tag_id     int8      not null,
    primary key (id)
)
create table system_permission
(
    id          bigserial not null,
    description varchar(255),
    permission  varchar(255),
    uuid        uuid,
    primary key (id)
)
create table tag
(
    id          bigserial not null,
    created_on  timestamp,
    description varchar(255),
    image       varchar(255),
    name        varchar(255),
    uuid        uuid,
    primary key (id)
)
create table tag_member_permission
(
    tag_id        int8 not null,
    user_id       int8 not null,
    permission_id int8 not null,
    primary key (tag_id, user_id, permission_id)
)
create table tag_member
(
    join_on timestamp,
    uuid    uuid,
    tag_id  int8 not null,
    user_id int8 not null,
    primary key (tag_id, user_id)
)
create table tag_permission
(
    id          bigserial not null,
    description varchar(255),
    tag_role    varchar(255),
    uuid        uuid,
    primary key (id)
)
create table user_blocked_tag
(
    uuid    uuid,
    tag_id  int8 not null,
    user_id int8 not null,
    primary key (tag_id, user_id)
)
create table user_group_blocked_tag
(
    uuid     uuid,
    group_id int8 not null,
    tag_id   int8 not null,
    user_id  int8 not null,
    primary key (group_id, tag_id, user_id)
)
create table verification_token
(
    id          bigserial not null,
    expiry_date timestamp,
    token       varchar(255),
    user_id     int8      not null,
    primary key (id)
)
alter table if exists comment add constraint FK97uyg6kb087m8a30f1jedx0uu foreign key (user_id) references pif_user
alter table if exists comment add constraint FKs1slvnkuemjsq2kj4h3vhx7i1 foreign key (post_id) references post
alter table if exists group_member add constraint FKjdm41jqs42ox7h6xj876eex14 foreign key (group_id) references pif_group
alter table if exists group_member add constraint FKvuueymq8y8g77k7nhgprkre1 foreign key (user_id) references pif_user
alter table if exists group_member_permission add constraint FK8bq16573d3gxkmo6h2nhky41v foreign key (permission_id) references group_permission
alter table if exists group_member_permission add constraint FK6mpyt5x4yke13bdfwwb8clyp2 foreign key (group_id, user_id) references group_member
alter table if exists group_post add constraint FKc8iydqtw4r8k1fn8h1l9f4ono foreign key (user_id) references pif_user
alter table if exists group_post add constraint FKgc5jngxpiw16mmtbufx5g4sii foreign key (group_id) references pif_group
alter table if exists group_post add constraint FKa1pgu2mojyejrkj2n8owovcev foreign key (tag_id) references group_tag
alter table if exists pif_like add constraint FKclmbni0bb79lvcac8lk6g9j6j foreign key (post_id) references post
alter table if exists pif_like add constraint FKmfo9yg9lauwfdyl07tjah6ig9 foreign key (user_id) references pif_user
alter table if exists pif_user add constraint FK8276dncx2217469f8ph40kbw0 foreign key (permission_id) references system_permission
alter table if exists post add constraint FKpun0ha8ni9og6fvml54vywn2a foreign key (user_id) references pif_user
alter table if exists post add constraint FKk1xnsd9end96dsdyxkw37ip1h foreign key (tag_id) references tag
alter table if exists tag_member_permission add constraint FKm2cc0yx8jw087lk9nkn3xvvsv foreign key (permission_id) references tag_permission
alter table if exists tag_member_permission add constraint FK74t5ue6kal1kfmglc7d4yvxpw foreign key (tag_id, user_id) references tag_member
alter table if exists tag_member add constraint FKk2236y6dfe8fq44iijclgr8wm foreign key (tag_id) references tag
alter table if exists tag_member add constraint FK9dx641nmdp3lcbe8fhca4mgnu foreign key (user_id) references pif_user
alter table if exists user_blocked_tag add constraint FKnv6m9i9t61ruh6c5b6qbix61j foreign key (tag_id) references tag
alter table if exists user_blocked_tag add constraint FKpoxwoy4i3ikgdclgpo8q5y166 foreign key (user_id) references pif_user
alter table if exists user_group_blocked_tag add constraint FK4m1ro64o43fy3nnysjs84qtjn foreign key (group_id) references pif_group
alter table if exists user_group_blocked_tag add constraint FKafayis5a06l70ht64om162a9j foreign key (tag_id) references tag
alter table if exists user_group_blocked_tag add constraint FK45uj31bj71pvxj7tb581e1xm3 foreign key (user_id) references pif_user
alter table if exists verification_token add constraint FKqrxubvdpu1f9bmfhj2vbuwima foreign key (user_id) references pif_user
