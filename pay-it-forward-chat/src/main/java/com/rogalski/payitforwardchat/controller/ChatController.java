package com.rogalski.payitforwardchat.controller;

import com.rogalski.payitforwardchat.configuration.AuthenticationFacade;
import com.rogalski.payitforwardchat.dto.ChatMessageDto;
import com.rogalski.payitforwardchat.dto.ChatRoomDto;
import com.rogalski.payitforwardchat.service.ChatMessageService;
import com.rogalski.payitforwardchat.service.ChatRoomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping()
@CrossOrigin(value = "*")
@Api(tags = "Chat")
public class ChatController {

    private final ChatMessageService chatMessageService;
    private final ChatRoomService chatRoomService;

    public ChatController (ChatMessageService chatMessageService, ChatRoomService chatRoomService) {
        this.chatMessageService = chatMessageService;
        this.chatRoomService = chatRoomService;
    }

    @MessageMapping("/process")
    public void processMessage (@Payload ChatMessageDto chatMessage) {
        chatMessageService.processMessage(chatMessage);
    }

    @GetMapping("/messages/{id}")
    @ApiOperation(value = "Fetch message by id.", notes = "Fetch message by id.", response = ChatMessageDto.class)
    public ResponseEntity<ChatMessageDto> findMessage (@PathVariable() Long id) {
        ChatMessageDto message = chatMessageService.findById(id, AuthenticationFacade.getAuthentication().getId());
        return new ResponseEntity<>(message, message != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @GetMapping("/rooms")
    @ApiOperation(value = "Fetch user rooms.", notes = "Fetch rooms for logged user.", response = ChatRoomDto[].class)
    public ResponseEntity<List<ChatRoomDto>> findRoomsByUser () {
        return ResponseEntity.ok(chatRoomService.getRoomsByUser(AuthenticationFacade.getAuthentication().getId()));
    }

    @GetMapping("/rooms/{roomId}")
    @ApiOperation(value = "Fetch messages from room.", notes = "Fetch messages from room.",
            response = ChatRoomDto[].class)
    public ResponseEntity<Page<ChatMessageDto>> findMessagesInRoom (@PathVariable Long roomId, Pageable pageable) {
        return ResponseEntity.ok(
                chatMessageService.getMessagesFromRoom(roomId, AuthenticationFacade.getAuthentication().getId(),
                        pageable));
    }
}
