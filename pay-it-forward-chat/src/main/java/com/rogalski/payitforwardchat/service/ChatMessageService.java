package com.rogalski.payitforwardchat.service;

import com.rogalski.payitforwardchat.dto.ChatMessageDto;
import com.rogalski.payitforwardchat.dto.ChatNotification;
import com.rogalski.payitforwardchat.dto.mapper.ChatMessageMapper;
import com.rogalski.payitforwardchat.model.ChatMessage;
import com.rogalski.payitforwardchat.model.ChatRoom;
import com.rogalski.payitforwardchat.model.MessageStatus;
import com.rogalski.payitforwardchat.repository.ChatMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class ChatMessageService {

    private final static ChatMessageMapper CHAT_MESSAGE_MAPPER = ChatMessageMapper.INSTANCE;
    private final static Logger log = LoggerFactory.getLogger(ChatMessageService.class);

    private final ChatMessageRepository chatMessageRepository;
    private final ChatRoomService chatRoomService;
    private final SimpMessagingTemplate messagingTemplate;

    public ChatMessageService (ChatMessageRepository chatMessageRepository, ChatRoomService chatRoomService,
                               SimpMessagingTemplate messagingTemplate) {
        this.chatMessageRepository = chatMessageRepository;
        this.chatRoomService = chatRoomService;
        this.messagingTemplate = messagingTemplate;
    }

    public void processMessage (ChatMessageDto chatMessageDto) {
        ChatRoom chatRoom =
                chatRoomService.getChatRoom(chatMessageDto.getSender().getId(), chatMessageDto.getRecipient().getId());
        ChatMessage chatMessage = CHAT_MESSAGE_MAPPER.chatMessageDtoToChatMessage(chatMessageDto);
        chatMessage.setChatRoom(chatRoom);

        ChatMessage saved = save(chatMessage);
        log.info("Message saved - sending to recipient");
        messagingTemplate.convertAndSendToUser(chatMessage.getChatRoom().getRecipient().getId().toString(),
                "/queue/messages",
                new ChatNotification(saved.getChatRoom().getSender().getUsername(), saved.getContent()));
    }

    private ChatMessage save (ChatMessage chatMessage) {
        chatMessage.setStatus(MessageStatus.RECEIVED);
        return chatMessageRepository.save(chatMessage);
    }

    public ChatMessageDto findById (Long id, Long userId) {
        return chatMessageRepository.findByIdAndUserId(id, userId).map(chatMessage -> {
            chatMessage.setStatus(MessageStatus.READ);
            return chatMessageRepository.save(chatMessage);
        }).map(cm -> {
            ChatMessageDto chatMessageDto = CHAT_MESSAGE_MAPPER.chatMessageToChatMessageDto(cm);
            chatMessageDto.setRecipient(cm.getChatRoom().getRecipient());
            chatMessageDto.setSender(cm.getChatRoom().getSender());
            return chatMessageDto;
        }).orElse(null);
    }

    public Page<ChatMessageDto> getMessagesFromRoom (Long roomId, Long userId, Pageable pageable) {
        return chatMessageRepository.findByRoomIdAndUserId(roomId, userId, pageable).map(message -> {
            ChatMessageDto chatMessageDto = CHAT_MESSAGE_MAPPER.chatMessageToChatMessageDto(message);
            chatMessageDto.setRecipient(message.getChatRoom().getRecipient());
            chatMessageDto.setSender(message.getChatRoom().getSender());
            return chatMessageDto;
        });
    }
}
