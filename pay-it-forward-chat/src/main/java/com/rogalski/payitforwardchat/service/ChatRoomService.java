package com.rogalski.payitforwardchat.service;

import com.rogalski.payitforwardchat.dto.ChatRoomDto;
import com.rogalski.payitforwardchat.dto.mapper.ChatRoomMapper;
import com.rogalski.payitforwardchat.exception.ResourceNotFound;
import com.rogalski.payitforwardchat.model.ChatRoom;
import com.rogalski.payitforwardchat.repository.ChatRoomRepository;
import com.rogalski.payitforwardchat.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ChatRoomService {

    private final ChatRoomMapper CHAT_ROOM_MAPPER = ChatRoomMapper.INSTANCE;
    private final static Logger log = LoggerFactory.getLogger(ChatRoomService.class);

    private final ChatRoomRepository chatRoomRepository;
    private final UserRepository userRepository;

    public ChatRoomService (ChatRoomRepository chatRoomRepository, UserRepository userRepository) {
        this.chatRoomRepository = chatRoomRepository;
        this.userRepository = userRepository;
    }

    public ChatRoom getChatRoom (Long senderId, Long recipientId) {
        Optional<ChatRoom> chatRoom = chatRoomRepository.findBySenderIdAndRecipientId(senderId, recipientId);
        if (chatRoom.isPresent()) {
            log.info("Found room for users: " + senderId + " and " + recipientId);
            return chatRoom.get();
        } else {
            log.info("Not found room for users: " + senderId + " and " + recipientId + ". Creating new one.");
            ChatRoom newChatRoom = new ChatRoom();
            newChatRoom.setSender(userRepository.findById(senderId).orElseThrow(
                    () -> new ResourceNotFound("Sender with id: " + senderId + " not found")));
            newChatRoom.setRecipient(userRepository.findById(recipientId).orElseThrow(
                    () -> new ResourceNotFound("Recipient with id: " + recipientId + " not found")));
            return chatRoomRepository.save(newChatRoom);
        }
    }

    public List<ChatRoomDto> getRoomsByUser (Long userId) {
        return chatRoomRepository.findByUserId(userId).stream().map(CHAT_ROOM_MAPPER::chatRoomToChatRoomDto)
                                 .collect(Collectors.toList());
    }
}
