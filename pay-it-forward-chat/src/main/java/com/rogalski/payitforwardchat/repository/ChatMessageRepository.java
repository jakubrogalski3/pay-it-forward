package com.rogalski.payitforwardchat.repository;

import com.rogalski.payitforwardchat.model.ChatMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {

    @Query("SELECT cm FROM ChatMessage cm LEFT JOIN ChatRoom cr ON cm.chatRoom.id = cr.id " +
            "WHERE cm.id = ?1 AND (cr.recipient.id = ?2 OR cr.sender.id = ?2)")
    Optional<ChatMessage> findByIdAndUserId (Long id, Long userId);

    @Query("SELECT cm FROM ChatMessage cm LEFT JOIN ChatRoom cr ON cm.chatRoom.id = cr.id " +
            "WHERE cr.id = ?1 AND (cr.recipient.id = ?2 or cr.sender.id = ?2) ORDER BY cm.timestamp DESC")
    Page<ChatMessage> findByRoomIdAndUserId (Long ChatRoomId, Long userId, Pageable pageable);
}
