package com.rogalski.payitforwardchat.repository;

import com.rogalski.payitforwardchat.model.ChatRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChatRoomRepository extends JpaRepository<ChatRoom, Long> {
    @Query("SELECT cr FROM ChatRoom cr WHERE cr.sender.id = ?1 AND cr.recipient.id = ?2" +
            "OR cr.recipient.id = ?1 AND cr.sender.id = ?2")
    Optional<ChatRoom> findBySenderIdAndRecipientId (Long senderId, Long recipientId);

    @Query("SELECT cr FROM ChatRoom cr WHERE cr.sender.id = ?1 or cr.recipient.id = ?1")
    List<ChatRoom> findByUserId (Long userId);
}
