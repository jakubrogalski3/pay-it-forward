package com.rogalski.payitforwardchat.dto;

import com.rogalski.payitforwardchat.model.MessageStatus;
import com.rogalski.payitforwardchat.model.User;
import lombok.Data;

import java.util.Date;

@Data
public class ChatMessageDto {
    private Long id;

    private User sender;

    private User recipient;

    private String content;

    private Date timestamp;

    private MessageStatus status;
}
