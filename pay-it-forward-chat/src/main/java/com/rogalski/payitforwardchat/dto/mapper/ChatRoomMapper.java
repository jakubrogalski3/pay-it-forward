package com.rogalski.payitforwardchat.dto.mapper;

import com.rogalski.payitforwardchat.dto.ChatRoomDto;
import com.rogalski.payitforwardchat.model.ChatRoom;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ChatRoomMapper {

    ChatRoomMapper INSTANCE = Mappers.getMapper(ChatRoomMapper.class);

    ChatRoomDto chatRoomToChatRoomDto (ChatRoom chatRoom);
}
