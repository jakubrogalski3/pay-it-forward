package com.rogalski.payitforwardchat.dto.mapper;

import com.rogalski.payitforwardchat.dto.ChatMessageDto;
import com.rogalski.payitforwardchat.model.ChatMessage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ChatMessageMapper {

    ChatMessageMapper INSTANCE = Mappers.getMapper(ChatMessageMapper.class);

    ChatMessageDto chatMessageToChatMessageDto (ChatMessage chatMessage);

    @Mapping(target = "chatRoom", ignore = true)
    ChatMessage chatMessageDtoToChatMessage (ChatMessageDto chatMessageDto);
}
