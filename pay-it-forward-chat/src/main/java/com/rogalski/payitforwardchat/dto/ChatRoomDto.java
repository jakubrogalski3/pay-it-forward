package com.rogalski.payitforwardchat.dto;

import com.rogalski.payitforwardchat.model.User;
import lombok.Data;

@Data
public class ChatRoomDto {

    private Long id;

    private User sender;

    private User recipient;
}
