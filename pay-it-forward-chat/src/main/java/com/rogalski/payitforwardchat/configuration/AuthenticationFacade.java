package com.rogalski.payitforwardchat.configuration;

import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationFacade {

    public static UserPrincipal getAuthentication () {
        return (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
