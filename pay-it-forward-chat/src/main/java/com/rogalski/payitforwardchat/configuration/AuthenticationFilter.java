package com.rogalski.payitforwardchat.configuration;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationFilter extends OncePerRequestFilter {

    private final static String ID_HEADER = "user_id";

    private final CustomUserDetailsService customUserDetailsService;

    public AuthenticationFilter (CustomUserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
    }

    @Override
    protected void doFilterInternal (HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (request.getRequestURI().startsWith("/chat-service/registry/") ||
                request.getRequestURI().startsWith("/chat-service/process")) {
            filterChain.doFilter(request, response);
            return;
        }
        String userIdHeader = request.getHeader(ID_HEADER);
        if (request.getRequestURI().contains("api-docs")) {

            filterChain.doFilter(request, response);
        }
        if (userIdHeader == null || userIdHeader.isEmpty()) {
            response.setStatus(401);
        } else {
            Long userId = Long.valueOf(userIdHeader);
            try {
                UserDetails userDetails = customUserDetailsService.loadUserById(userId);
                UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(),
                                userDetails.getAuthorities());

                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                filterChain.doFilter(request, response);
            } catch (RuntimeException e) {
                response.setStatus(403);
            }
        }
    }
}
