package com.rogalski.payitforwardchat.configuration;

import com.rogalski.payitforwardchat.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserPrincipal implements UserDetails {

    private final Long id;
    private final String username;

    public UserPrincipal (Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public static UserPrincipal createUser (User user) {
        return new UserPrincipal(user.getId(), user.getUsername());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities () {
        return null;
    }

    @Override
    public String getPassword () {
        return null;
    }

    public Long getId () {
        return id;
    }

    @Override
    public String getUsername () {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired () {
        return true;
    }

    @Override
    public boolean isAccountNonLocked () {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired () {
        return true;
    }

    @Override
    public boolean isEnabled () {
        return true;
    }
}
