package com.rogalski.payitforwardchat.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class ChatRoom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User sender;

    @ManyToOne
    private User recipient;

    @OneToMany(mappedBy = "chatRoom")
    @OrderBy(value = "timestamp DESC")
    private Set<ChatMessage> chatMessages;

}
