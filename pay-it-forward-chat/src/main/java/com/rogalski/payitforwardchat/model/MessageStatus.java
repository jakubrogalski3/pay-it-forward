package com.rogalski.payitforwardchat.model;

public enum MessageStatus {
    RECEIVED, READ
}
