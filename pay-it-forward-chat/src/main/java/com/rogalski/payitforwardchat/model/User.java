package com.rogalski.payitforwardchat.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "pif_user")
@Data
public class User {

    @Id
    private Long id;

    private String username;
}
