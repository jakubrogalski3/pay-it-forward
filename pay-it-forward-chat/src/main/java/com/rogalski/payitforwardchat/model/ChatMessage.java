package com.rogalski.payitforwardchat.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class ChatMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private ChatRoom chatRoom;

    private String content;

    private Date timestamp;

    @Enumerated(EnumType.STRING)
    private MessageStatus status;
}
