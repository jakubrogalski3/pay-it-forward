package com.rogalski.payitforwardchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@SpringBootApplication
@EnableEurekaClient
public class PayItForwardChatApplication {

    public static void main (String[] args) {
        SpringApplication.run(PayItForwardChatApplication.class, args);
    }

    @Bean
    public Docket docket () {
        return new Docket(DocumentationType.OAS_30).apiInfo(
                                                           new ApiInfoBuilder().title("Pay it forward chat API").version("0.0.1-SNAPSHOT").build())
                                                   .tags(new Tag("Chat", "Endpoints for chat operations")).select()
                                                   .apis(RequestHandlerSelectors.withClassAnnotation(
                                                           RestController.class)).build();
    }

}
