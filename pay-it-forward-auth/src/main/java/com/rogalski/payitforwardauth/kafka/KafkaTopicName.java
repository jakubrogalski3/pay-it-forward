package com.rogalski.payitforwardauth.kafka;

public enum KafkaTopicName {
    VERIFICATION_TOPIC("verification-topic"), REGISTRATION_TOPIC("registration-topic");

    private String topicName;

    KafkaTopicName (String topicName) {
        this.topicName = topicName;
    }

    public String topicName () {
        return topicName;
    }
}
