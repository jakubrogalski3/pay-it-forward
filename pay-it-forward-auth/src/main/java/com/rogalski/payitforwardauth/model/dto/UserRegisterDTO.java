package com.rogalski.payitforwardauth.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
public class UserRegisterDTO {

    private String firstName;

    private String lastName;

    private String username;

    private String email;

    private String password;

    @Builder
    public UserRegisterDTO (String firstName, String lastName, String username, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
