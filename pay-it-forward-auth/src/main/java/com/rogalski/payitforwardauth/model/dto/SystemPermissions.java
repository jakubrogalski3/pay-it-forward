package com.rogalski.payitforwardauth.model.dto;

public enum SystemPermissions {
    ADMIN("ADMIN"), MODERATOR("MODERATOR"), SYSTEM_USER("SYSTEM_USER");

    private final String name;

    private SystemPermissions (String systemPermission) {
        name = systemPermission;
    }

    @Override
    public String toString () {
        return name;
    }
}
