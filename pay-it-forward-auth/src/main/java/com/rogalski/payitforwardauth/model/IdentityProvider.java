package com.rogalski.payitforwardauth.model;

public enum IdentityProvider {
    LOCAL, GOOGLE
}
