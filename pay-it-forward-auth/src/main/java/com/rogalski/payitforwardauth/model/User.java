package com.rogalski.payitforwardauth.model;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "pif_user")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private final UUID uuid = UUID.randomUUID();

    private boolean isEnabled;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    @Enumerated(EnumType.STRING)
    private IdentityProvider identityProvider;

    @NotNull
    private String email;

    private final Date createdOn = new Date();

    @OneToMany()
    @JoinTable(name = "user_permission")
    private Set<SystemPermission> permission;

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof User))
            return false;
        User user = (User) o;
        return getUuid().equals(user.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
