package com.rogalski.payitforwardauth.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserMailDto {

    private String email;

    private String username;

    private String token;

}
