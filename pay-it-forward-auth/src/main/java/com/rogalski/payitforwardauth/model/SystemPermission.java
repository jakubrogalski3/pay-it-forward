package com.rogalski.payitforwardauth.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;
import java.util.UUID;

@Entity
@Data
public class SystemPermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private final UUID uuid = UUID.randomUUID();

    private String permission;

    private String description;

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof SystemPermission))
            return false;
        SystemPermission that = (SystemPermission) o;
        return getUuid().equals(that.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
