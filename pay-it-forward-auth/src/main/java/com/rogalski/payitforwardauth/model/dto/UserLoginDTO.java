package com.rogalski.payitforwardauth.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
public class UserLoginDTO {

    private String username;

    private String password;

    @Builder
    public UserLoginDTO (String username, String password) {
        this.username = username;
        this.password = password;
    }
}
