package com.rogalski.payitforwardauth.model.dto;

import lombok.Data;

@Data
public class GoogleUserRegisterDto {

    private String clientId;

    private String token;

}
