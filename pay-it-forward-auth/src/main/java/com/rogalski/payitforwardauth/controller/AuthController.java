package com.rogalski.payitforwardauth.controller;

import com.rogalski.payitforwardauth.model.dto.UserLoginDTO;
import com.rogalski.payitforwardauth.model.dto.UserRegisterDTO;
import com.rogalski.payitforwardauth.service.AuthService;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jwt")
@Api(tags = "JWT")
public class AuthController {

    private final AuthService authService;

    public AuthController (AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login (@RequestBody UserLoginDTO userDTO) {
        String token = authService.login(userDTO);
        return new ResponseEntity<>(token, token != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/register")
    public ResponseEntity<Void> register (@RequestBody UserRegisterDTO userDTO) {
        authService.register(userDTO);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
