package com.rogalski.payitforwardauth.controller;

import com.rogalski.payitforwardauth.model.dto.GoogleUserRegisterDto;
import com.rogalski.payitforwardauth.service.GoogleOAuth2Service;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
@RequestMapping("/oauth2")
@Api(tags = "OAuth2")
public class OAuth2Controller {

    private final GoogleOAuth2Service googleOAuth2Service;

    public OAuth2Controller (GoogleOAuth2Service googleOAuth2Service) {
        this.googleOAuth2Service = googleOAuth2Service;
    }

    @PostMapping("/google")
    public ResponseEntity<String> processGoogleAccessToken (@RequestBody GoogleUserRegisterDto userRegisterDto)
            throws GeneralSecurityException, IOException {
        String token = googleOAuth2Service.processAccessToken(userRegisterDto);
        return new ResponseEntity<>(token, token != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
