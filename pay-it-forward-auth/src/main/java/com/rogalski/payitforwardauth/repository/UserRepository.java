package com.rogalski.payitforwardauth.repository;

import com.rogalski.payitforwardauth.model.IdentityProvider;
import com.rogalski.payitforwardauth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername (String username);

    Optional<User> findByEmailAndIdentityProvider (String email, IdentityProvider identityProvider);

    Optional<User> findByEmail (String email);
}
