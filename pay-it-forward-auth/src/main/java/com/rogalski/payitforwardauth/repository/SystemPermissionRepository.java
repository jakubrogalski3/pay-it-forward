package com.rogalski.payitforwardauth.repository;

import com.rogalski.payitforwardauth.model.SystemPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SystemPermissionRepository extends JpaRepository<SystemPermission, Long> {

    Optional<SystemPermission> findByPermission (String name);

}
