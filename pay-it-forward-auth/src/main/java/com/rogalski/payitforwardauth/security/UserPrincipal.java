package com.rogalski.payitforwardauth.security;

import com.rogalski.payitforwardauth.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserPrincipal implements UserDetails {

    private final Long id;
    private final String username;
    private final Boolean isEnabled;
    private final String password;
    private final Collection<? extends GrantedAuthority> authorities;

    public UserPrincipal (Long id, String username, String password, Boolean isEnabled,
                          Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.isEnabled = isEnabled;
        this.authorities = authorities;
    }

    public static UserPrincipal createUser (User user, String roles) {

        return new UserPrincipal(user.getId(), user.getUsername(), user.getPassword(), user.isEnabled(),
                getAuthorities(roles));
    }

    public static UserPrincipal createUser (User user) {

        return new UserPrincipal(user.getId(), user.getUsername(), user.getPassword(), user.isEnabled(),
                getAuthorities("ROLE_ADMIN"));
    }

    private static Collection<UserAuthorities> getAuthorities (String roles) {
        if (StringUtils.hasText(roles)) {
            List<String> rolesList = Arrays.asList(roles.split(","));
            return rolesList.stream().map(UserAuthorities::new).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities () {
        return this.authorities;
    }

    public Long getId () {
        return id;
    }

    @Override
    public String getPassword () {
        return this.password;
    }

    @Override
    public String getUsername () {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired () {
        return true;
    }

    @Override
    public boolean isAccountNonLocked () {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired () {
        return true;
    }

    @Override
    public boolean isEnabled () {
        return isEnabled;
    }
}
