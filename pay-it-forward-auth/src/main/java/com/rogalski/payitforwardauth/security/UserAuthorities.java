package com.rogalski.payitforwardauth.security;

import org.springframework.security.core.GrantedAuthority;

import java.util.Objects;

public class UserAuthorities implements GrantedAuthority {

    String authority;

    UserAuthorities (String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority () {
        return authority;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof UserAuthorities))
            return false;
        UserAuthorities that = (UserAuthorities) o;
        return getAuthority().equals(that.getAuthority());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getAuthority());
    }
}
