package com.rogalski.payitforwardauth.security;

import com.rogalski.payitforwardauth.model.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@Component
public class JwtTokenProvider {

    @Value("${pif-api.jwt.secret}")
    private String jwtSecret;

    @Value("${pif-api.jwt.expirationTime}")
    private int jwtExpirationInMs;

    public String generateToken (User user) {
        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder().setSubject(Long.toString(user.getId())).setIssuedAt(now).setExpiration(expirationDate)
                   .signWith(SignatureAlgorithm.HS256, jwtSecret.getBytes(StandardCharsets.UTF_8)).compact();
    }

    public String generateToken (Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder().setSubject(Long.toString(userPrincipal.getId())).setIssuedAt(now)
                   .setExpiration(expirationDate).signWith(SignatureAlgorithm.HS256, jwtSecret).compact();
    }

}
