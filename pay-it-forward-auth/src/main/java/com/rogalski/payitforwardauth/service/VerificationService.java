package com.rogalski.payitforwardauth.service;

import com.rogalski.payitforwardauth.model.User;
import com.rogalski.payitforwardauth.model.VerificationToken;
import com.rogalski.payitforwardauth.repository.UserRepository;
import com.rogalski.payitforwardauth.repository.VerificationTokenRepository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

@Service
public class VerificationService {

    private final VerificationTokenRepository verificationTokenRepository;
    private final UserRepository userRepository;

    public VerificationService (VerificationTokenRepository verificationTokenRepository,
                                UserRepository userRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
        this.userRepository = userRepository;
    }

    public void createVerificationToken (User user, String token) {
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(user);
        verificationToken.setToken(token);
        verificationToken.setExpiryDate(Date.from(Instant.now().plus(2, ChronoUnit.DAYS)));
        verificationTokenRepository.save(verificationToken);
    }

    public Boolean verifyUser (String token) {
        User user;
        VerificationToken verificationToken;
        Optional<VerificationToken> verificationTokenOptional = verificationTokenRepository.findByToken(token);
        if (verificationTokenOptional.isPresent()) {
            verificationToken = verificationTokenOptional.get();
            user = verificationTokenOptional.get().getUser();
        } else {
            return null;
        }
        if (verificationToken.getExpiryDate().compareTo(Date.from(Instant.now())) < 0) {
            user.setEnabled(true);
            verificationTokenRepository.delete(verificationToken);
            userRepository.save(user);
        } else {
            throw new RuntimeException("Verification token expired");
        }
        return user.isEnabled();
    }

    public void refreshVerificationToken (String token) {
        VerificationToken verificationToken = verificationTokenRepository.findByToken(token).orElseThrow(
                () -> new RuntimeException("Token: + " + token + " not found"));
        verificationTokenRepository.delete(verificationToken);
    }
}
