package com.rogalski.payitforwardauth.service;

import com.rogalski.payitforwardauth.model.User;
import com.rogalski.payitforwardauth.repository.UserRepository;
import com.rogalski.payitforwardauth.security.UserPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    public UserService (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername (String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s).orElseThrow(() -> new RuntimeException("User not found"));
        return UserPrincipal.createUser(user);
    }

    UserDetails loadUserById (Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        return UserPrincipal.createUser(user);
    }

}
