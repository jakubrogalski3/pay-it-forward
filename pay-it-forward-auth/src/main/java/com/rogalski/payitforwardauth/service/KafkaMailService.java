package com.rogalski.payitforwardauth.service;

import com.rogalski.payitforwardauth.kafka.KafkaTopicName;
import com.rogalski.payitforwardauth.model.dto.UserMailDto;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaMailService {

    private final KafkaTemplate<String, UserMailDto> kafkaTemplate;

    public KafkaMailService (KafkaTemplate<String, UserMailDto> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendVerificationEmailMessage (UserMailDto user) {
        kafkaTemplate.send(KafkaTopicName.VERIFICATION_TOPIC.topicName(), user);
    }

    public void sendInitialEmailMessage (UserMailDto user) {
        kafkaTemplate.send(KafkaTopicName.REGISTRATION_TOPIC.topicName(), user);
    }
}
