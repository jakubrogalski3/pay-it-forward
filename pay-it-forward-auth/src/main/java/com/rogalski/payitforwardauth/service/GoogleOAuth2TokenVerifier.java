package com.rogalski.payitforwardauth.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.rogalski.payitforwardauth.model.dto.GoogleUserRegisterDto;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Service
public class GoogleOAuth2TokenVerifier {

    public GoogleIdToken.Payload verifyToken (GoogleUserRegisterDto googleUser)
            throws GeneralSecurityException, IOException {

        GoogleIdTokenVerifier verifier =
                new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new GsonFactory()).setAudience(
                        Collections.singletonList(googleUser.getClientId())).build();

        GoogleIdToken idToken = verifier.verify(googleUser.getToken());
        if (idToken != null) {
            return idToken.getPayload();

        } else {
            throw new RuntimeException("401 - wrong access token");
        }
    }
}
