package com.rogalski.payitforwardauth.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.rogalski.payitforwardauth.model.IdentityProvider;
import com.rogalski.payitforwardauth.model.User;
import com.rogalski.payitforwardauth.model.dto.GoogleUserRegisterDto;
import com.rogalski.payitforwardauth.model.dto.SystemPermissions;
import com.rogalski.payitforwardauth.model.dto.UserMailDto;
import com.rogalski.payitforwardauth.repository.SystemPermissionRepository;
import com.rogalski.payitforwardauth.repository.UserRepository;
import com.rogalski.payitforwardauth.security.JwtTokenProvider;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Optional;
import java.util.Set;

@Service
public class GoogleOAuth2Service {

    private final UserRepository userRepository;
    private final JwtTokenProvider tokenProvider;
    private final GoogleOAuth2TokenVerifier googleTokenVerifier;
    private final SystemPermissionRepository systemPermissionRepository;
    private final KafkaMailService kafkaMailService;

    public GoogleOAuth2Service (UserRepository userRepository, JwtTokenProvider tokenProvider,
                                GoogleOAuth2TokenVerifier googleTokenVerifier,
                                SystemPermissionRepository systemPermissionRepository,
                                KafkaMailService kafkaMailService) {
        this.userRepository = userRepository;
        this.tokenProvider = tokenProvider;
        this.googleTokenVerifier = googleTokenVerifier;
        this.systemPermissionRepository = systemPermissionRepository;
        this.kafkaMailService = kafkaMailService;
    }

    public String processAccessToken (GoogleUserRegisterDto googleUser) throws GeneralSecurityException, IOException {
        GoogleIdToken.Payload payload = googleTokenVerifier.verifyToken(googleUser);
        String email = payload.getEmail();
        Optional<User> userOptional = userRepository.findByEmail(email);
        if (userOptional.isPresent() && userOptional.get().getIdentityProvider().equals(IdentityProvider.GOOGLE)) {
            return tokenProvider.generateToken(userOptional.get());
        } else if (userOptional.isEmpty()) {
            User createdUser = createUser(payload);
            return tokenProvider.generateToken(createdUser);
        } else {
            throw new RuntimeException("User with that email is registered in traditional way");
        }
    }

    private User createUser (GoogleIdToken.Payload payload) {
        String email = payload.getEmail();
        String familyName = (String) payload.get("family_name");
        String givenName = (String) payload.get("given_name");

        User user = new User();
        user.setEmail(email);
        user.setUsername(email);
        user.setEnabled(true);
        user.setIdentityProvider(IdentityProvider.GOOGLE);
        user.setFirstName(givenName);
        user.setLastName(familyName);
        user.setPermission(Set.of(systemPermissionRepository.findByPermission(SystemPermissions.SYSTEM_USER.toString())
                                                            .orElseThrow(() -> new RuntimeException(
                                                                    "Permission not found"))));

        user = userRepository.save(user);

        kafkaMailService.sendInitialEmailMessage(
                UserMailDto.builder().username(user.getUsername()).email(user.getEmail()).build());

        return user;
    }
}
