package com.rogalski.payitforwardauth.service;

import com.rogalski.payitforwardauth.model.IdentityProvider;
import com.rogalski.payitforwardauth.model.User;
import com.rogalski.payitforwardauth.model.dto.SystemPermissions;
import com.rogalski.payitforwardauth.model.dto.UserLoginDTO;
import com.rogalski.payitforwardauth.model.dto.UserMailDto;
import com.rogalski.payitforwardauth.model.dto.UserRegisterDTO;
import com.rogalski.payitforwardauth.repository.SystemPermissionRepository;
import com.rogalski.payitforwardauth.repository.UserRepository;
import com.rogalski.payitforwardauth.security.JwtTokenProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider tokenProvider;
    private final SystemPermissionRepository systemPermissionRepository;
    private final UserRepository userRepository;
    private final KafkaMailService kafkaMailService;

    public AuthService (AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder,
                        JwtTokenProvider tokenProvider, SystemPermissionRepository systemPermissionRepository,
                        UserRepository userRepository, KafkaMailService kafkaMailService) {
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
        this.systemPermissionRepository = systemPermissionRepository;
        this.userRepository = userRepository;
        this.kafkaMailService = kafkaMailService;
    }

    public String login (UserLoginDTO user) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return tokenProvider.generateToken(authentication);
    }

    public void register (UserRegisterDTO userDTO) {
        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setIdentityProvider(IdentityProvider.LOCAL);
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setPermission(Set.of(systemPermissionRepository.findByPermission(SystemPermissions.SYSTEM_USER.toString())
                                                            .orElseThrow(() -> new RuntimeException(
                                                                    "Permission not found"))));

        user = userRepository.save(user);
        if (user.getId() != null) {
            kafkaMailService.sendVerificationEmailMessage(
                    UserMailDto.builder().email(user.getEmail()).username(user.getUsername()).token("testtoken")
                               .build());
        } else {
            throw new RuntimeException("Create user error");
        }

    }

}
