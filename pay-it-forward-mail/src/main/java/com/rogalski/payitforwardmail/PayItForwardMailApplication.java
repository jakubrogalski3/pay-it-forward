package com.rogalski.payitforwardmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties.class)
public class PayItForwardMailApplication {

    public static void main (String[] args) {
        SpringApplication.run(PayItForwardMailApplication.class, args);
    }

}
