package com.rogalski.payitforwardmail;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "pif-api")
@Data
public class ApplicationProperties {

    private Email email;

    @Data
    public static class Email {
        private String verificationEmailTitle;
        private String verificationEmailTemplate;

        private String registrationEmailTitle;
        private String registrationEmailTemplate;

        private String changePasswordTitle;
        private String changePasswordTemplate;
    }
}
