package com.rogalski.payitforwardmail.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserMailDto {

    private String email = "";

    private String username = "";

    private String token = "";

}
