package com.rogalski.payitforwardmail.kafka;

import com.rogalski.payitforwardmail.ApplicationProperties;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class KafkaAuthListener {

    private final static String REGEX_TO_FILL = "#";
    private final JavaMailSender mailSender;
    private final ApplicationProperties properties;

    public KafkaAuthListener (JavaMailSender mailSender, ApplicationProperties properties) {
        this.mailSender = mailSender;
        this.properties = properties;
    }

    @KafkaListener(topics = "registration-topic", groupId = "pifGroup",
            containerFactory = "userKafkaListenerContainerFactory")
    void registrationListener (UserMailDto user) {
        String message = properties.getEmail().getRegistrationEmailTemplate().replaceFirst(REGEX_TO_FILL,
                user.getUsername() != null ? user.getUsername() : user.getEmail());

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject(properties.getEmail().getRegistrationEmailTitle());
        email.setText(message);
        mailSender.send(email);
    }

    @KafkaListener(topics = "verification-topic", groupId = "pifGroup",
            containerFactory = "userKafkaListenerContainerFactory")
    void verificationListener (UserMailDto user) {
        String message = properties.getEmail().getVerificationEmailTemplate().replaceFirst(REGEX_TO_FILL,
                user.getUsername() != null ? user.getUsername() : user.getEmail()).replaceFirst(REGEX_TO_FILL, "token");

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject(properties.getEmail().getVerificationEmailTitle());
        email.setText(message);
        mailSender.send(email);
    }

    @KafkaListener(topics = "change-password-topic", groupId = "pifGroup",
            containerFactory = "userKafkaListenerContainerFactory")
    public void changePasswordListener (UserMailDto user) {
        String message = properties.getEmail().getChangePasswordTemplate().replaceFirst(REGEX_TO_FILL,
                                           user.getUsername() != null ? user.getUsername() : user.getEmail())
                                   .replaceFirst(REGEX_TO_FILL, "user.getUuid().toString()");

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject(properties.getEmail().getChangePasswordTitle());
        email.setText(message);
        mailSender.send(email);
    }
}
