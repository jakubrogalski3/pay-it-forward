package com.rogalski.payitforwardmail.kafka;

public enum KafkaTopicName {
    REGISTRATION_TOPIC("registration-topic");

    private final String topicName;

    KafkaTopicName (String topicName) {
        this.topicName = topicName;
    }

    public String topicName () {
        return topicName;
    }
}
