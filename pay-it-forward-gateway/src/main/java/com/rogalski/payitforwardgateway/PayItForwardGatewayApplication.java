package com.rogalski.payitforwardgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PayItForwardGatewayApplication {

    public static void main (String[] args) {
        SpringApplication.run(PayItForwardGatewayApplication.class, args);
    }

}
