package com.rogalski.payitforwardgateway.service;

import com.rogalski.payitforwardgateway.model.SystemPermission;
import com.rogalski.payitforwardgateway.model.User;
import com.rogalski.payitforwardgateway.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetUserRolesService {

    private static final String GROUP_PERMISSION_PREFIX = "GROUP";
    private static final String TAG_PERMISSION_PREFIX = "TAG";

    private final UserRepository userRepository;

    public GetUserRolesService (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String getUserPermissions (String userId) {
        Long id = Long.valueOf(userId);
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        List<String> permissions = new ArrayList<>();
        String systemPermissions =
                user.getPermission().stream().map(SystemPermission::getPermission).collect(Collectors.joining(","));
        permissions.add(systemPermissions);

        if (!user.getGroups().isEmpty()) {
            String groupPermissions = user.getGroups().stream()
                                          .map(groupMember -> groupMember.getGroupPermissions().stream()
                                                                         .map(groupPermission ->
                                                                                 GROUP_PERMISSION_PREFIX + "_" +
                                                                                         groupMember.getId()
                                                                                                    .getGroupId() +
                                                                                         "_" +
                                                                                         groupPermission.getGroupRole())
                                                                         .collect(Collectors.joining(",")))
                                          .collect(Collectors.joining(","));
            permissions.add(groupPermissions);
        }

        if (!user.getTags().isEmpty()) {
            String tagPermissions = user.getTags().stream().map(tagMember -> tagMember.getTagPermissions().stream()
                                                                                      .map(tagPermission ->
                                                                                              TAG_PERMISSION_PREFIX +
                                                                                                      "_" +
                                                                                                      tagMember.getId()
                                                                                                               .getTagId() +
                                                                                                      "_" +
                                                                                                      tagPermission.getTagRole())
                                                                                      .collect(Collectors.joining(",")))
                                        .collect(Collectors.joining(","));
            permissions.add(tagPermissions);
        }

        return String.join(",", permissions);
    }

}
