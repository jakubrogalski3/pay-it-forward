package com.rogalski.payitforwardgateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@Configuration
@Primary
public class SwaggerConfig implements SwaggerResourcesProvider {

    @Autowired
    private RouteLocator routeLocator;

    @Override
    public List<SwaggerResource> get () {
        List<SwaggerResource> resources = new ArrayList<>();
        routeLocator.getRoutes().subscribe(route -> {
            String[] service = route.getUri().toString().split("//");
            if (service.length > 1 &&
                    resources.stream().noneMatch(res -> res.getName().equals(service[1].toLowerCase()))) {
                resources.add(swaggerResource(service[1].toLowerCase(), "/" + service[1].toLowerCase() + "/v3/api-docs",
                        "1.0"));
            }
        });

        return resources;
    }

    private SwaggerResource swaggerResource (final String name, final String location, final String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }

}
