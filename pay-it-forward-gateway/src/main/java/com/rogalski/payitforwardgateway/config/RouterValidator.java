package com.rogalski.payitforwardgateway.config;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

@Component
public class RouterValidator {

    public static final List<Route> openApiEndpoints = List.of(new Route("/auth-service/jwt/register", HttpMethod.POST),
            new Route("/auth-service/jwt/login", HttpMethod.POST),
            new Route("/auth-service/oauth2/google", HttpMethod.POST),
            new Route("/chat-service/registry", HttpMethod.GET), new Route("/chat-service/process", HttpMethod.GET),
            new Route("/core-service/posts/users", HttpMethod.GET),
            new Route("/core-service/v3/api-docs", HttpMethod.GET),
            new Route("/chat-service/v3/api-docs", HttpMethod.GET),
            new Route("/auth-service/v3/api-docs", HttpMethod.GET));
    private final String API_PREFIX = "/api/v1";
    public Predicate<ServerHttpRequest> isSecured = request -> openApiEndpoints.stream().noneMatch(
            route -> request.getURI().getPath().startsWith(API_PREFIX + route.url) &&
                    Objects.equals(request.getMethod(), route.method));

    @AllArgsConstructor
    public static class Route {
        private final String url;
        private final HttpMethod method;
    }

}
