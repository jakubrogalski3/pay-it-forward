package com.rogalski.payitforwardgateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    private final AuthenticationFilter filter;

    public GatewayConfig (AuthenticationFilter filter) {
        this.filter = filter;
    }

    @Bean
    public RouteLocator routes (RouteLocatorBuilder builder) {
        return builder.routes().route("core-service",
                              r -> r.path("/api/v1/core-service/**").filters(f -> f.filter(filter)).uri("lb://CORE-SERVICE"))

                      .route("auth-service", r -> r.path("/api/v1/auth-service/**").filters(f -> f.filter(filter))
                                                   .uri("lb://AUTH-SERVICE"))

                      .route("chat-service", r -> r.path("/api/v1/chat-service/**").filters(f -> f.filter(filter))
                                                   .uri("lb:ws://CHAT-SERVICE")).build();
    }
}
