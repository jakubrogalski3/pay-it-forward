package com.rogalski.payitforwardgateway.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtUtil {

    @Value("${pif-api.jwt.secret}")
    private String jwtSecret;

    public Claims getAllClaimsFromToken (String token) {
        return Jwts.parser().setSigningKey(this.jwtSecret).parseClaimsJws(token).getBody();
    }

    private boolean isTokenExpired (String token) {
        return this.getAllClaimsFromToken(token).getExpiration().before(new Date());
    }

    public boolean isInvalid (String token) {
        return this.isTokenExpired(token);
    }
}
