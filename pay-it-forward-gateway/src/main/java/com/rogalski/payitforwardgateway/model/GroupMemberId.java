package com.rogalski.payitforwardgateway.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class GroupMemberId implements Serializable {

    @Column(name = "group_id")
    private Long groupId;
    @Column(name = "user_id")
    private Long userId;

    public GroupMemberId () {
    }

    public GroupMemberId (Long groupId, Long userId) {
        this.groupId = groupId;
        this.userId = userId;
    }
}
