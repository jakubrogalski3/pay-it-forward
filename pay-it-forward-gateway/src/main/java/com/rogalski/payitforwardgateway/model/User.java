package com.rogalski.payitforwardgateway.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "pif_user")
@NamedEntityGraphs({
        @NamedEntityGraph(name = "graph.UserSystemPermission", attributeNodes = {
                @NamedAttributeNode(value = "permission"),
                @NamedAttributeNode(value = "groups", subgraph = "groupPermissions"),
                @NamedAttributeNode(value = "tags", subgraph = "tagPermissions")
        }, subgraphs = {
                @NamedSubgraph(name = "groupPermissions", attributeNodes = @NamedAttributeNode("groupPermissions")),
                @NamedSubgraph(name = "tagPermissions", attributeNodes = @NamedAttributeNode("tagPermissions"))
        })
})
@Data
public class User {
    private final UUID uuid = UUID.randomUUID();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany()
    @JoinTable(name = "user_permission")
    private Set<SystemPermission> permission;

    @OneToMany()
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Set<GroupMember> groups;

    @OneToMany()
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Set<TagMember> tags;

}
