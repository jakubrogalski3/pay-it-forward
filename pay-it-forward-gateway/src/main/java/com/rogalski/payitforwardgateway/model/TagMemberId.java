package com.rogalski.payitforwardgateway.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class TagMemberId implements Serializable {

    @Column(name = "tag_id")
    private Long tagId;
    @Column(name = "user_id")
    private Long userId;

    public TagMemberId () {
    }

    public TagMemberId (Long tagId, Long userId) {
        this.tagId = tagId;
        this.userId = userId;
    }

}
