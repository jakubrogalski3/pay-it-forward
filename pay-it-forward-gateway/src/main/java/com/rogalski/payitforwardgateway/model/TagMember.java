package com.rogalski.payitforwardgateway.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "tag_member")
@Data
public class TagMember {

    private final UUID uuid = UUID.randomUUID();
    @EmbeddedId
    private TagMemberId id;
    private Date joinOn;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tag_member_permission", joinColumns = {
            @JoinColumn(name = "tag_id", referencedColumnName = "tag_id"),
            @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    }, inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private Set<TagPermission> tagPermissions;

    @Override
    public boolean equals (Object o) {
        if (this == o)
            return true;
        if (!(o instanceof TagMember))
            return false;
        TagMember tagMember = (TagMember) o;
        return getUuid().equals(tagMember.getUuid());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getUuid());
    }
}
