create table comment (id  bigserial not null, content varchar(255), created_on timestamp, uuid uuid, user_id int8, post_id int8, primary key (id));
create table group_member (join_on timestamp, uuid uuid, group_id int8 not null, user_id int8 not null, primary key (group_id, user_id));
create table group_member_permission (group_id int8 not null, user_id int8 not null, permission_id int8 not null, primary key (group_id, user_id, permission_id));
create table group_permission (id  bigserial not null, description varchar(255), group_role varchar(255), uuid uuid, primary key (id));
create table pif_group (id  bigserial not null, created_on timestamp, name varchar(255), uuid uuid, primary key (id));
create table pif_like (id  bigserial not null, created_on timestamp, uuid uuid, post_id int8, user_id int8, primary key (id));
create table pif_user (id  bigserial not null, created_on timestamp, email varchar(255), first_name varchar(255), identity_provider varchar(255), is_enabled boolean not null, last_name varchar(255), password varchar(255), username varchar(255), uuid uuid, permission_id int8, primary key (id));
create table post (id  bigserial not null, content varchar(255), created_on timestamp, uuid uuid, user_id int8, group_id int8, tag_id int8 not null, primary key (id));
create table system_permission (id  bigserial not null, description varchar(255), permission varchar(255), uuid uuid, primary key (id));
create table tag (id  bigserial not null, created_on timestamp, description varchar(255), image varchar(255), name varchar(255), uuid uuid, primary key (id));
create table tag_member_permission (tag_id int8 not null, user_id int8 not null, permission_id int8 not null, primary key (tag_id, user_id, permission_id));
create table tag_member (join_on timestamp, uuid uuid, tag_id int8 not null, user_id int8 not null, primary key (tag_id, user_id));
create table tag_permission (id  bigserial not null, description varchar(255), tag_role varchar(255), uuid uuid, primary key (id));
create table user_blocked_tag (uuid uuid, tag_id int8 not null, user_id int8 not null, primary key (tag_id, user_id));
create table user_group_blocked_tag (uuid uuid, group_id int8 not null, tag_id int8 not null, user_id int8 not null, primary key (group_id, tag_id, user_id));
create table verification_token (id  bigserial not null, expiry_date timestamp, token varchar(255), user_id int8 not null, primary key (id));
alter table if exists comment add constraint FK97uyg6kb087m8a30f1jedx0uu foreign key (user_id) references pif_user;
alter table if exists comment add constraint FKs1slvnkuemjsq2kj4h3vhx7i1 foreign key (post_id) references post;
alter table if exists group_member add constraint FKjdm41jqs42ox7h6xj876eex14 foreign key (group_id) references pif_group;
alter table if exists group_member add constraint FKvuueymq8y8g77k7nhgprkre1 foreign key (user_id) references pif_user;
alter table if exists group_member_permission add constraint FK8bq16573d3gxkmo6h2nhky41v foreign key (permission_id) references group_permission;
alter table if exists group_member_permission add constraint FK6mpyt5x4yke13bdfwwb8clyp2 foreign key (group_id, user_id) references group_member;
alter table if exists pif_like add constraint FKclmbni0bb79lvcac8lk6g9j6j foreign key (post_id) references post;
alter table if exists pif_like add constraint FKmfo9yg9lauwfdyl07tjah6ig9 foreign key (user_id) references pif_user;
alter table if exists pif_user add constraint FK8276dncx2217469f8ph40kbw0 foreign key (permission_id) references system_permission;
alter table if exists post add constraint FKpun0ha8ni9og6fvml54vywn2a foreign key (user_id) references pif_user;
alter table if exists post add constraint FKd5i8o7s62aky02cvjcynkt05b foreign key (group_id) references pif_group;
alter table if exists post add constraint FKk1xnsd9end96dsdyxkw37ip1h foreign key (tag_id) references tag;
alter table if exists tag_member_permission add constraint FKm2cc0yx8jw087lk9nkn3xvvsv foreign key (permission_id) references tag_permission;
alter table if exists tag_member_permission add constraint FK74t5ue6kal1kfmglc7d4yvxpw foreign key (tag_id, user_id) references tag_member;;
alter table if exists tag_member add constraint FKk2236y6dfe8fq44iijclgr8wm foreign key (tag_id) references tag;
alter table if exists tag_member add constraint FK9dx641nmdp3lcbe8fhca4mgnu foreign key (user_id) references pif_user;
alter table if exists user_blocked_tag add constraint FKnv6m9i9t61ruh6c5b6qbix61j foreign key (tag_id) references tag;
alter table if exists user_blocked_tag add constraint FKpoxwoy4i3ikgdclgpo8q5y166 foreign key (user_id) references pif_user;
alter table if exists user_group_blocked_tag add constraint FK4m1ro64o43fy3nnysjs84qtjn foreign key (group_id) references pif_group;
alter table if exists user_group_blocked_tag add constraint FKafayis5a06l70ht64om162a9j foreign key (tag_id) references tag;
alter table if exists user_group_blocked_tag add constraint FK45uj31bj71pvxj7tb581e1xm3 foreign key (user_id) references pif_user;
alter table if exists verification_token add constraint FKqrxubvdpu1f9bmfhj2vbuwima foreign key (user_id) references pif_user;

create or replace function get_user_post_feed(userid bigint, pageSize integer, pageOffset bigint)
    returns table
            (
                id             bigint,
                content        varchar(255),
                created_on     timestamp,
                uuid           uuid,
                tag_name       varchar(255),
                username       varchar(255),
                group_name     varchar(255),
                group_id       bigint,
                group_tag_name varchar(255)
            )
    language plpgsql
as
$$
begin
    return query
        select p.id, p.content, p.created_on, p.uuid, t.name, u.username, null, null, null
        from post p
                 join tag_member tm on tm.tag_id = p.tag_id and tm.user_id = $1
                 join tag t on p.tag_id = t.id
                 join pif_user u on p.user_id = u.id
        where p.user_id != $1
          and not exists(select 1 from user_blocked_tag ubt where ubt.user_id = $1 and ubt.tag_id = p.tag_id)
        union all
        select gp.id, gp.content, gp.created_on, gp.uuid, null, u.username, pg.name, pg.id, gt.name
        from group_post gp
                 join group_member gm on gp.group_id = gm.group_id and gm.user_id = $1
                 join group_tag gt on gp.group_tag_id = gt.id
                 join pif_user u on gp.user_id = u.id
                 join pif_group pg on gp.group_id = pg.id
        where gp.user_id != $1
          and not exists(select 1
                         from user_group_blocked_tag ugbt
                         where ugbt.tag_id = gp.group_tag_id
                           and ugbt.user_id = $1
                           and ugbt.group_id = gp.group_id)
    order by created_on desc limit pageSize offset pageOffset;
end;$$

create or replace function get_user_post_feed_count(userid bigint)
    returns integer
    language plpgsql
as
$$
begin
    return (
        select count(*) from (
        select p.id
        from post p
        where p.user_id != $1
          and not exists(select 1 from user_blocked_tag ubt where ubt.user_id = $1 and ubt.tag_id = p.tag_id)
        union all
        select gp.id
        from group_post gp
        where gp.user_id != $1
          and not exists(select 1
                         from user_group_blocked_tag ugbt
                         where ugbt.tag_id = gp.group_tag_id
                           and ugbt.user_id = $1
                           and ugbt.group_id = gp.group_id)) as post_feed);
end;$$

create or replace function get_user_posts(userid bigint, viewerId bigint, pagesize integer, pageoffset bigint)
    returns TABLE(id bigint, content character varying, created_on timestamp without time zone, uuid uuid, tag_name character varying, username character varying, group_name character varying, group_tag_name character varying)
    language plpgsql
as $$
begin
    return query
        select p.id, p.content, p.created_on, p.uuid, t.name, u.username, null, null
        from post p
                 join tag t on p.tag_id = t.id
                 join pif_user u on p.user_id = u.id
        where p.user_id = $1
          and not exists(select 1 from user_blocked_tag ubt where ubt.user_id = $2 and ubt.tag_id = p.tag_id)
        union all
        select gp.id, gp.content, gp.created_on, gp.uuid, null, u.username, pg.name, gt.name
        from group_post gp
                 join group_member gm on gp.group_id = gm.group_id and gm.user_id = $2
                 join group_tag gt on gp.group_tag_id = gt.id
                 join pif_user u on gp.user_id = u.id
                 join pif_group pg on gp.group_id = pg.id
        where gp.user_id = $1
          and not exists(select 1
                         from user_group_blocked_tag ugbt
                         where ugbt.tag_id = gp.group_tag_id
                           and ugbt.user_id = $2
                           and ugbt.group_id = gp.group_id)
        order by created_on desc limit pageSize offset pageOffset;
end;
$$;

create or replace function get_user_posts_count(userid bigint, viewerId bigint) returns integer
    language plpgsql
as $$
begin
    return (
        select count(*) from (
                                 select p.id
                                 from post p
                                 where p.user_id = $1
                                   and not exists(select 1 from user_blocked_tag ubt where ubt.user_id = $2 and ubt.tag_id = p.tag_id)
                                 union all
                                 select gp.id
                                 from group_post gp
                                          join group_member gm on gp.group_id = gm.group_id and gm.user_id = $2
                                 where gp.user_id = $1
                                   and not exists(select 1
                                                  from user_group_blocked_tag ugbt
                                                  where ugbt.tag_id = gp.group_tag_id
                                                    and ugbt.user_id = $2
                                                    and ugbt.group_id = gp.group_id)) as post_feed);
end;
$$;

insert into system_permission (description, permission, uuid) values ('system admin', 'SYSTEM_ADMIN', '7beadc20-1657-493a-b8a0-298c1ead4212');
insert into system_permission (description, permission, uuid) values ('system user', 'SYSTEM_USER', '7beadc20-1657-493a-b8a0-298c1ead4211');

insert into group_permission (description, group_role, uuid) values ('group admin', 'ADMIN', '7beadc20-1657-493a-b8a0-298c1ead4213');
insert into group_permission (description, group_role, uuid) values ('group user', 'MEMBER', '7beadc20-1657-493a-b8a0-298c1ead4214');

insert into tag_permission (description, tag_role, uuid) values ('tag user', 'MEMBER', '7beadc20-1657-493a-b8a0-298c1ead4215');
insert into tag_permission (description, tag_role, uuid) values ('tag admin', 'ADMIN', '7beadc20-1657-493a-b8a0-298c1ead4216');

insert into pif_user (created_on, first_name, last_name, password, username, uuid, email, is_enabled, identity_provider, permission_id)
values ('2021-04-19 17:41:37.000000', 'Admin', 'Admin', '$2a$10$0kESOz/ol1Gluqh74U/oCul/0cRz/u3Ok5Z04XCU99lD8r5jv1kMe', 'admin', '95b45260-ca7e-498e-aff0-745ef35a0dfa', 'admin@admin.admin', true, 'LOCAL', 1);
insert into pif_user (created_on, first_name, last_name, password, username, uuid, email, is_enabled, identity_provider, permission_id)
values ('2021-04-19 17:41:38.000000', 'User', 'User', '$2a$10$0kESOz/ol1Gluqh74U/oCul/0cRz/u3Ok5Z04XCU99lD8r5jv1kMe', 'user', '95b45260-ca7e-498e-aff0-745ef35a0df0', 'user@user.user', true, 'LOCAL', 2);
