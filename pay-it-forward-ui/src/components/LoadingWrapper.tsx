import { ReactNode } from 'react'

import { Loading } from '../pages/types'

type LoadingWrapperProps = {
  loading: Loading
  text?: string
  children: ReactNode
}

const LoadingWrapper = ({
  loading,
  text,
  children,
}: LoadingWrapperProps) => {

  const body: ReactNode = loading === 'pending' ?
    <p>{text || 'Loading...'}</p> : (
      loading === 'succeeded' ? <>{children}</> : <p>Failed</p>
    )

  return body
}

export default LoadingWrapper
