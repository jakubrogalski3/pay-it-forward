const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function(app) {
  app.use(
    '/api/v1/',
    createProxyMiddleware({
      target: 'http://localhost:8080',
      changeOrigin: true,
      headers: {
        Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNjY0Mzg5MjAzLCJleHAiOjE2NjUyNTMyMDN9.I_-ic38Tl9X2oVWdBxo8iLxGX4fq3PVqLN3aF6mOwF4',
      },
    }),
  )
}
