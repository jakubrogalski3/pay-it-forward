import i18next from 'i18next'
import React from 'react'
import './App.css'
import { initReactI18next } from 'react-i18next'
import { Route, Routes } from 'react-router-dom'

import MainPage from './pages/MainPage'
import PostDetails from './pages/posts/PostDetails'
import PostForm from './pages/posts/PostForm'
import PostsList from './pages/posts/PostsList'

const enTranslations = {
  'list': 'list',
}

const plTranslations = {
  'list': 'lista',
}

i18next
  .use(initReactI18next)
  .init({
    lng: 'pl',
    debug: true,
    resources: {
      en: {
        translation: enTranslations,
      },
      pl: {
        translation: plTranslations,
      },
    },
  })

function App () {

  return (<div>
      <MainPage />
      <Routes>
        <Route path={'/'} element={<PostsList />} />
        <Route path={'/posts/:postType/:postId'} element={<PostDetails />} />
        <Route path={'/posts/:postType/:group/:postId'} element={<PostDetails />} />
        <Route path={'/posts/add'} element={<PostForm />} />
      </Routes>
    </div>
  )
}

export default App
