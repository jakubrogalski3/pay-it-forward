import { ReactNode, useEffect } from 'react'
import { useParams } from 'react-router-dom'

import { useAppDispatch, useAppSelector } from '../../hooks'

import { fetchPost } from './store/postSlice'

const PostDetails = () => {
  const {
    loadingPost,
    post,
  } = useAppSelector(state => state.posts)
  const dispatch = useAppDispatch()
  const {
    postType,
    group,
    postId,
  } = useParams()

  useEffect(() => {
    dispatch(fetchPost({
      postType,
      group,
      postId,
    }))
  }, [dispatch])

  const body: ReactNode = loadingPost === 'pending' || loadingPost === 'idle' ?
    <p>Loading...</p> : (loadingPost === 'succeeded' ? <p>{post?.content}</p> :
      <p>Failed</p>)

  return <div>{body}</div>
}

export default PostDetails
