import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import axios from '../../../api/axios'
import { Loading } from '../../types'
import { CreatePost, PostDetailsType, PostType, UserGroup } from '../post.types'

export type PostsStateType = {
  loadingPosts: Loading,
  posts: PostType[]
  totalPosts: number

  loadingPost: Loading,
  post: PostDetailsType | undefined

  userPosts: PostType[]
  userTotalPosts: number
  loadingUserPost: Loading

  savingPost: Loading,

  loadingUserGroups: Loading,
  userGroups: UserGroup[]
}

const initialState: PostsStateType = {
  loadingPosts: 'idle',
  posts: [],
  totalPosts: 0,

  loadingPost: 'idle',
  post: undefined,

  userPosts: [],
  userTotalPosts: 0,
  loadingUserPost: 'idle',

  savingPost: 'idle',

  loadingUserGroups: 'idle',
  userGroups: [],
}

export const fetchPosts = createAsyncThunk('posts/fetchPosts', async () => {
  const response = await axios.get('/core-service/posts/feed')
  return response.data
})

export const fetchUserPosts = createAsyncThunk('posts/fetchUserPosts', async () => {
  const response = await axios.get('/core-service/posts/users/myself')
  return response.data
})

// todo move to other slice
export const fetchUserGroups = createAsyncThunk('groups/fetchPosts', async () => {
  const response = await axios.get('/core-service/groups')
  return response.data
})

type FetchPostParam = {
  postType?: string
  group?: string
  postId?: string
}

export const fetchPost = createAsyncThunk('posts/fetchPost',
  async ({
    postType,
    group,
    postId,
  }: FetchPostParam) => {
    if (postType === 'group' && group) {
      const response = await axios.get(`/core-service/group-posts/groups/${group}/posts/${postId}`)
      return response.data
    } else if (postType) {
      const response = await axios.get(`/core-service/posts/${postId}`)
      return response.data
    }
  })

type CreatePostParam = {
  post: CreatePost
}

export const createPost = createAsyncThunk('posts/create', async ({ post }: CreatePostParam) => {
  await axios.post(`/core-service/posts/`, post)
})

export const postSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(fetchPost.pending, (state) => {
        state.loadingPost = 'pending'
      })
      .addCase(fetchPost.fulfilled, (state, action) => {
        state.post = action.payload
        state.loadingPost = 'succeeded'
      })
      .addCase(fetchPost.rejected, (state) => {
        state.loadingPost = 'failed'
      })
      .addCase(fetchUserPosts.pending, (state) => {
        state.loadingUserPost = 'pending'
      })
      .addCase(fetchUserPosts.fulfilled, (state, action) => {
        state.userPosts = action.payload.content
        state.userTotalPosts = action.payload.totalPosts
        state.loadingUserPost = 'succeeded'
      })
      .addCase(fetchUserPosts.rejected, (state) => {
        state.loadingUserPost = 'failed'
      })
      .addCase(fetchPosts.pending, state => {
        state.totalPosts = 0
        state.loadingPosts = 'pending'
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.posts = action.payload.content
        state.totalPosts = action.payload.totalElements
        state.loadingPosts = 'succeeded'
      })
      .addCase(fetchPosts.rejected, state => {
        state.savingPost = 'failed'
      })
      .addCase(createPost.pending, state => {
        state.savingPost = 'pending'
      })
      .addCase(createPost.fulfilled, (state, action) => {
        state.savingPost = 'succeeded'
      })
      .addCase(createPost.rejected, state => {
        state.loadingPosts = 'failed'
      })

      .addCase(fetchUserGroups.pending, state => {
        state.loadingUserGroups = 'pending'
      })
      .addCase(fetchUserGroups.fulfilled, (state, action) => {
        state.userGroups = action.payload
        state.loadingUserGroups = 'succeeded'
      })
      .addCase(fetchUserGroups.rejected, state => {
        state.loadingUserGroups = 'failed'
      })
  },
})

// Action creators are generated for each case reducer function
// export const {} = postSlice.actions

export default postSlice.reducer

