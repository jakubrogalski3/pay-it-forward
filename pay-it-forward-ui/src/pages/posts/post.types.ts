type PostTypeValue = 'Global' | 'Group'

export type PostType = {
  id: number
  content: string
  username: string
  tag: string,
  group: string,
  groupId: number,
  comments: any[],
  tagCausing: string,
  groupCausing: string,
  postType: PostTypeValue
}

export type PostDetailsType = {
  id: number
  content: string
  username: string
  tag: string,
  group: string,
  groupId: number,
  comments: any[],
  tagCausing: string,
  groupCausing: string,
  postType: PostTypeValue
}

export type CreatePost = {
  content: string,
  groupId?: number
  tagId?: number
}

// todo move to other file

export type UserGroup = {
  id: number,
  name: string
}
