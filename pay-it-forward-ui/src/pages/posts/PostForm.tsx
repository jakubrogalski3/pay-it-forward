import { Button, FormControl, FormGroup, InputLabel, MenuItem, Select, TextareaAutosize } from '@mui/material'
import { useEffect, useState } from 'react'

import { useAppDispatch, useAppSelector } from '../../hooks'

import { createPost, fetchUserGroups } from './store/postSlice'

const PostForm = () => {

  const {
    userGroups,
    loadingUserGroups,
  } = useAppSelector(state => state.posts)

  const dispatch = useAppDispatch()

  const [content, setContent] = useState<string>('')
  const [group, setGroup] = useState<string>()

  useEffect(() => {
    dispatch(fetchUserGroups())
  }, [])

  const submitPost = () => {
    dispatch(createPost({
      post: {
        content,
        tagId: 1,
      },
    }))
  }

  return (
    <FormGroup>

      <FormControl fullWidth>

        <TextareaAutosize
          value={content}
          minRows={3}
          onChange={(value) => setContent(value.target.value)}
        />
      </FormControl>

      {loadingUserGroups !== 'succeeded' ? <p>Loading...</p> : <FormControl fullWidth>
        <InputLabel id='demo-simple-select-label'>Group</InputLabel>
        <Select
          labelId='demo-simple-select-label'
          id='demo-simple-select'
          value={group}
          label='Group'
          onChange={(value) => {
            setGroup(value.target.value)
          }}
        >
          {userGroups.map(group =>
            (<MenuItem value={group.id}>{group.name}</MenuItem>),
          )}
        </Select>
      </FormControl>}

      <FormControl>
        <Button type={'submit'} onClick={() => submitPost()}>Submit</Button>
      </FormControl>

    </FormGroup>
  )

}

export default PostForm
