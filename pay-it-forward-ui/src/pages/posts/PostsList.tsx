import { Grid } from '@mui/material'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'

import LoadingWrapper from '../../components/LoadingWrapper'
import { useAppDispatch, useAppSelector } from '../../hooks'

import { PostType } from './post.types'
import { fetchPosts, fetchUserPosts } from './store/postSlice'

const PostsList = () => {
  const {
    loadingPosts,
    posts,
    loadingUserPost,
    userPosts,
  } = useAppSelector(state => state.posts)
  const {
    i18n,
    t,
  } = useTranslation()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  useEffect(() => {
    dispatch(fetchPosts())
    dispatch(fetchUserPosts())
  }, [])

  const generateNavigation = (post: PostType) => {
    if (post.postType === 'Group') {
      return `/posts/group/${post.groupId}/${post.id}`
    } else {
      return `/posts/global/${post.id}`
    }
  }

  const changeLanguage = async () => {
    await i18n.changeLanguage(i18n.language === 'en' ? 'pl' : 'en')
  }

  return <Grid container>
    <Grid item xs={6}>
      <LoadingWrapper loading={loadingPosts}>
        <p>{t('list')}</p>
        <ul>
          {posts.map(post => (
            <li key={post.id}>
              <a onClick={() => navigate(generateNavigation(post))}>{post.content}</a>
            </li>
          ))}
        </ul>
        <button onClick={() => changeLanguage()}>{i18n.language === 'en' ? 'PL' : 'ENG'}</button>
      </LoadingWrapper>
    </Grid>

    <Grid item>
      <LoadingWrapper loading={loadingUserPost}>
        <p>{t('list')}</p>
        <ul>
          {userPosts.map(post => (
            <li key={post.id}>
              <a onClick={() => navigate(generateNavigation(post))}>{post.content}</a>
            </li>
          ))}
        </ul>
        <button onClick={() => changeLanguage()}>{i18n.language === 'en' ? 'PL' : 'ENG'}</button>
      </LoadingWrapper>
    </Grid>

  </Grid>
}

export default PostsList
